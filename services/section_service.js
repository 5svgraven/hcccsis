const baseUrl = "https://hccc-sis.herokuapp.com/api/section";

export async function CreateSection(name, maxStudent, createdBy) {
  const res = await fetch(`${baseUrl}`, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${localStorage.getItem("token")}`,
    },
    body: JSON.stringify({
      name: name,
      maxStudent: maxStudent,
      createdBy: createdBy,
    }),
  });
  const data = await res.json();
  return data;
}
export async function UpdateSection(sectionId, name, maxStudent) {
  const res = await fetch(`${baseUrl}`, {
    method: "PUT",
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${localStorage.getItem("token")}`,
    },
    body: JSON.stringify({
      sectionId: sectionId,
      name: name,
      maxStudent: maxStudent,
    }),
  });
  const data = await res.json();
  return data;
}

export async function EnrollStudentToClass(sectionId) {
  const res = await fetch(`${baseUrl}/enroll-student-class`, {
    method: "PUT",
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${localStorage.getItem("token")}`,
    },
    body: JSON.stringify({
      sectionId: sectionId,
    }),
  });
  const data = await res.json();
  return data;
}

export async function AssignSubjectSection(sectionId, subjectId) {
  const res = await fetch(`${baseUrl}/assign-section`, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${localStorage.getItem("token")}`,
    },
    body: JSON.stringify({
      sectionId: sectionId,
      subjectId: subjectId,
    }),
  });
  const data = await res.json();
  return data;
}

export async function GetAllSection() {
  const res = await fetch(`${baseUrl}`, {
    headers: {
      Authorization: `Bearer ${localStorage.getItem("token")}`,
    },
  });
  const data = await res.json();
  return data;
}
export async function GetOneSection(sectionId) {
  const res = await fetch(`${baseUrl}/${sectionId}`, {
    headers: {
      Authorization: `Bearer ${localStorage.getItem("token")}`,
    },
  });
  const data = await res.json();
  return data;
}

export async function DeleteSection(sectionId) {
  const res = await fetch(`${baseUrl}/${sectionId}`, {
    method: "DELETE",
    headers: {
      Authorization: `Bearer ${localStorage.getItem("token")}`,
    },
  });
  const data = await res.json();

  return data;
}
