const baseUrl = "https://hccc-sis.herokuapp.com/api";

export async function CreateCourse(name, userId) {
  const res = await fetch(`${baseUrl}/course`, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${localStorage.getItem("token")}`,
    },
    body: JSON.stringify({
      name: name,
      createdBy: userId,
    }),
  });
  const data = await res.json();
  return data;
}

export async function UpdateCourse(courseId, name) {
  const res = await fetch(`${baseUrl}/course`, {
    method: "PUT",
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${localStorage.getItem("token")}`,
    },
    body: JSON.stringify({
      courseId: courseId,
      name: name,
    }),
  });
  const data = await res.json();
  return data;
}
export async function GetAllCourse() {
  const res = await fetch(`${baseUrl}/course/`, {
    headers: {
      Authorization: `Bearer ${localStorage.getItem("token")}`,
    },
  });
  const data = await res.json();
  return data;
}

export async function EnrollStudentToCourse(courseId) {
  const res = await fetch(`${baseUrl}/course/enroll-student-course`, {
    method: "PUT",
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${localStorage.getItem("token")}`,
    },
    body: JSON.stringify({
      courseId: courseId,
    }),
  });
  const data = await res.json();
  return data;
}

export async function AssignSubjectCourse(courseId, subjectId) {
  const res = await fetch(`${baseUrl}/course/assign-course`, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${localStorage.getItem("token")}`,
    },
    body: JSON.stringify({
      courseId: courseId,
      subjectId: subjectId,
    }),
  });
  const data = await res.json();
  return data;
}

export async function GetOneCourse(courseId) {
  const res = await fetch(`${baseUrl}/course/${courseId}`, {
    headers: {
      Authorization: `Bearer ${localStorage.getItem("token")}`,
    },
  });
  const data = await res.json();
  data;
  return data;
}

export async function DeleteCourse(courseId) {
  const res = await fetch(`${baseUrl}/course/${courseId}`, {
    method: "DELETE",
    headers: {
      Authorization: `Bearer ${localStorage.getItem("token")}`,
    },
  });
  const data = await res.json();
  return data;
}
