const baseUrl = "https://hccc-sis.herokuapp.com/api";

export async function CreateStudent(
  firstname,
  lastname,
  age,
  gender,
  birthday,
  address,
  email,
  mobileNo,
  parentsName,
  parentsMobileNo,
  username,
  password,
  requirements
) {
  const res = await fetch(`${baseUrl}/student`, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${localStorage.getItem("token")}`,
    },
    body: JSON.stringify({
      firstname: firstname,
      lastname: lastname,
      age: age,
      gender: gender,
      birthday: birthday,
      address: address,
      email: email,
      mobileNo: mobileNo,
      parentsName: parentsName,
      parentMobileNo: parentsMobileNo,
      username: username,
      password: password,
      requirements: requirements,
    }),
  });
  const data = await res.json();
  return data;
}

export async function GetDetails() {
  const res = await fetch(`${baseUrl}/student/details`, {
    headers: {
      Authorization: `Bearer ${localStorage.getItem("token")}`,
    },
  });
  const data = res.json();
  return data;
}

export async function CheckUser(username) {
  const res = await fetch(`${baseUrl}/admin/check-username-exist`, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${localStorage.getItem("token")}`,
    },
    body: JSON.stringify({
      username: username,
    }),
  });
  const data = await res.json();
  return data;
}

export async function LoginStudent(username, password) {
  const res = await fetch(`${baseUrl}/student/login`, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${localStorage.getItem("token")}`,
    },
    body: JSON.stringify({
      username: username,
      password: password,
    }),
  });
  const data = await res.json();
  return data;
}

export async function GetAllStudent() {
  const res = await fetch(`${baseUrl}/student`, {
    headers: {
      Authorization: `Bearer ${localStorage.getItem("token")}`,
    },
  });
  const data = await res.json();
  return data;
}

export async function GetOneStudent(studentId) {
  const res = await fetch(`${baseUrl}/student/${studentId}`, {
    headers: {
      Authorization: `Bearer ${localStorage.getItem("token")}`,
    },
  });
  const data = await res.json();
  return data;
}

export async function UpdateStudent(
  studentId,
  firstname,
  lastname,
  age,
  address,
  email,
  mobileNo
) {
  const res = await fetch(`${baseUrl}/student`, {
    method: "PUT",
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${localStorage.getItem("token")}`,
    },
    body: JSON.stringify({
      studentId: studentId,
      firstname: firstname,
      lastname: lastname,
      age: age,
      address: address,
      email: email,
      mobileNo: mobileNo,
    }),
  });
  const data = await res.json();
  return data;
}

export async function DeleteStudent(studentId) {
  const res = await fetch(`${baseUrl}/student/${studentId}`, {
    method: "DELETE",
    headers: {
      Authorization: `Bearer ${localStorage.getItem("token")}`,
    },
  });
  const data = await res.json();
  return data;
}
