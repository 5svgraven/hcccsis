const baseUrl = "https://hccc-sis.herokuapp.com/api/grades";

export async function CreateGrade(grade, referenceId, studentId, teacherId) {
  const res = await fetch(`${baseUrl}`, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${localStorage.getItem("token")}`,
    },
    body: JSON.stringify({
      grade: grade,
      referenceId: referenceId,
      studentId: studentId,
      teacherId: teacherId,
    }),
  });
  const data = await res.json();
  return data;
}
export async function UpdateGrade(gradeId, grade) {
  const res = await fetch(`${baseUrl}`, {
    method: "PUT",
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${localStorage.getItem("token")}`,
    },
    body: JSON.stringify({
      gradeId: gradeId,
      grade: grade,
    }),
  });
  const data = await res.json();
  return data;
}

export async function GetAllGrade() {
  const res = await fetch(`${baseUrl}`, {
    headers: {
      Authorization: `Bearer ${localStorage.getItem("token")}`,
    },
  });
  const data = await res.json();
  return data;
}

export async function GetAllSubjectGrades(subjectId) {
  const res = await fetch(`${baseUrl}/subject/${subjectId}`, {
    headers: {
      Authorization: `Bearer ${localStorage.getItem("token")}`,
    },
  });
  const data = await res.json();
  return data;
}

export async function GetAllSubjectStudent(studentId) {
  const res = await fetch(`${baseUrl}/student/${studentId}`, {
    headers: {
      Authorization: `Bearer ${localStorage.getItem("token")}`,
    },
  });
  const data = await res.json();
  return data;
}

export async function GetOneGrade(gradeId) {
  const res = await fetch(`${baseUrl}/${gradeId}`, {
    headers: {
      Authorization: `Bearer ${localStorage.getItem("token")}`,
    },
  });
  const data = await res.json();
  return data;
}
export async function DeleteGrade(gradeId) {
  const res = await fetch(`${baseUrl}/${gradeId}`, {
    method: "DELETE",
    headers: {
      Authorization: `Bearer ${localStorage.getItem("token")}`,
    },
  });
  const data = await res.json();
  return data;
}
