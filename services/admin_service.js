const baseUrl = "https://hccc-sis.herokuapp.com/api/admin";

export async function CreateAdmin(
  firstname,
  lastname,
  age,
  gender,
  role,
  birthday,
  address,
  mobileNo,
  username,
  password
) {
  const res = await fetch(`${baseUrl}`, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${localStorage.getItem("token")}`,
    },
    body: JSON.stringify({
      firstname: firstname,
      lastname: lastname,
      age: age,
      gender: gender,
      role: role,
      birthday: birthday,
      address: address,
      mobileNo: mobileNo,
      username: username,
      password: password,
    }),
  });
  const data = await res.json();
  return data;
}

export async function GetDetailsAdmin() {
  const res = await fetch(`${baseUrl}/details`, {
    headers: {
      Authorization: `Bearer ${localStorage.getItem("token")}`,
    },
  });
  const data = res.json();
  return data;
}

export async function UpdateAdmin(
  adminId,
  firstname,
  lastname,
  age,
  birthday,
  address,
  mobileNo
) {
  const res = await fetch(`${baseUrl}`, {
    method: "PUT",
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${localStorage.getItem("token")}`,
    },
    body: JSON.stringify({
      adminId: adminId,
      firstname: firstname,
      lastname: lastname,
      age: age,
      birthday: birthday,
      address: address,
      mobileNo: mobileNo,
    }),
  });
  const data = await res.json();
  return data;
}
export async function GetAllAdmin() {
  const res = await fetch(`${baseUrl}`, {
    headers: {
      Authorization: `Bearer ${localStorage.getItem("token")}`,
    },
  });
  const data = await res.json();
  return data;
}
export async function GetOneAdmin(adminId) {
  const res = await fetch(`${baseUrl}/${adminId}`, {
    headers: {
      Authorization: `Bearer ${localStorage.getItem("token")}`,
    },
  });
  const data = await res.json();
  return data;
}
export async function DeleteAdmin(adminId) {
  const res = await fetch(`${baseUrl}/${adminId}`, {
    method: "DELETE",
    headers: {
      Authorization: `Bearer ${localStorage.getItem("token")}`,
    },
  });
  const data = await res.json();
  return data;
}

export async function SendEmailToApplicant(message, fullName, emailTo) {
  const res = await fetch(`${baseUrl}/send-email`, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${localStorage.getItem("token")}`,
    },
    body: JSON.stringify({
      message: message,
      nameFrom: fullName,
      emailTo: emailTo,
    }),
  });
  const data = await res.json();
  return data;
}
