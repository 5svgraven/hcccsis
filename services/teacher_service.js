const baseUrl = "https://hccc-sis.herokuapp.com/api/teacher";

export async function CreateTeacher(
  firstname,
  lastname,
  age,
  gender,
  birthday,
  address,
  mobileNo,
  specialization,
  username,
  password
) {
  const res = await fetch(`${baseUrl}`, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${localStorage.getItem("token")}`,
    },
    body: JSON.stringify({
      firstname: firstname,
      lastname: lastname,
      age: age,
      gender: gender,
      birthday: birthday,
      address: address,
      mobileNo: mobileNo,
      specialization: specialization,
      username: username,
      password: password,
    }),
  });
  const data = await res.json();
  return data;
}

export async function UpdateTeacher(
  teacherId,
  firstname,
  lastname,
  age,
  address,
  mobileNo,
  specialization
) {
  const res = await fetch(`${baseUrl}`, {
    method: "PUT",
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${localStorage.getItem("token")}`,
    },
    body: JSON.stringify({
      teacherId: teacherId,
      firstname: firstname,
      lastname: lastname,
      age: age,
      address: address,
      mobileNo: mobileNo,
      specialization: specialization,
    }),
  });
  const data = await res.json();
  return data;
}

export async function GetDetailsTeacher() {
  const res = await fetch(`${baseUrl}/details`, {
    headers: {
      Authorization: `Bearer ${localStorage.getItem("token")}`,
    },
  });
  const data = res.json();
  return data;
}

export async function LoginTeacher(username, password) {
  const res = await fetch(`${baseUrl}/login`, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${localStorage.getItem("token")}`,
    },
    body: JSON.stringify({
      username: username,
      password: password,
    }),
  });
  const data = await res.json();
  return data;
}

export async function GetAllTeacher() {
  const res = await fetch(`${baseUrl}`, {
    headers: {
      Authorization: `Bearer ${localStorage.getItem("token")}`,
    },
  });
  const data = await res.json();
  return data;
}
export async function GetOneTeacher(teacherId) {
  const res = await fetch(`${baseUrl}/${teacherId}`, {
    headers: {
      Authorization: `Bearer ${localStorage.getItem("token")}`,
    },
  });
  const data = await res.json();
  return data;
}
export async function DeleteTeacher(teacherId) {
  const res = await fetch(`${baseUrl}/${teacherId}`, {
    method: "DELETE",
    headers: {
      Authorization: `Bearer ${localStorage.getItem("token")}`,
    },
  });
  const data = await res.json();
  return data;
}
