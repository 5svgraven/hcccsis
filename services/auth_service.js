const baseUrl = "https://hccc-sis.herokuapp.com/api";

export async function loginStudent() {
  const res = await fetch(`${baseUrl}/student/login`, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify({
      username: username,
      password: password,
    }),
  });
  const data = await res.json();
  if (data) {
    localStorage.setItem("token", data.access);
  }
  return data;
}

export async function loginTeacher() {
  const res = await fetch(`${baseUrl}/teacher/login`, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify({
      username: username,
      password: password,
    }),
  });
  const data = await res.json();
  if (data) {
    localStorage.setItem("token", data.access);
  }
  return data;
}

export async function loginAdmin(username, password) {
  const res = await fetch(`${baseUrl}/admin/login`, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify({
      username: username,
      password: password,
    }),
  });
  const data = await res.json();
  return data;
}

export async function userDetails(user, token) {
  let userType = "";
  if (user == "admin") userType = "admin/details";
  else if (user == "teacher") userType = "teacher/details";
  else if (user == "student") userType = "student/details";

  const res = await fetch(`${baseUrl}/${userType}`, {
    headers: {
      Authorization: token,
    },
  });
  const data = await res.json();
  return data;
}

export function createAdmin() {}

export function updateTeacher() {}
export function updateStudent() {}
export function updateAdmin() {}
