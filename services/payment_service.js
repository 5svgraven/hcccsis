const baseUrl = "https://hccc-sis.herokuapp.com/api/payment";

export async function CreatePayment(name, amount, studentId, remarks, adminId) {
  const res = await fetch(`${baseUrl}`, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${localStorage.getItem("token")}`,
    },
    body: JSON.stringify({
      name: name,
      amount: amount,
      studentId: studentId,
      remarks: remarks,
      createdBy: adminId,
    }),
  });
  const data = await res.json();
  return data;
}

export async function UpdatePayment(studentId, remarks, paymentId) {
  if (remarks === "Pre-Enrollment") {
    const res = await fetch(`${baseUrl}`, {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
      body: JSON.stringify({
        studentId: studentId,
        status: true,
        remarks: "Paid Pre-Enrollment",
        paymentId: paymentId,
        studentStatus: "Not Enrolled",
      }),
    });
    const data = await res.json();
    return data;
  } else {
    const res = await fetch(`${baseUrl}`, {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
      body: JSON.stringify({
        studentId: studentId,
        status: true,
        remarks: remarks,
        paymentId: paymentId,
        studentStatus: "Enrolled",
      }),
    });
    const data = await res.json();
    return data;
  }
}

export async function GetAllPayment() {
  const res = await fetch(`${baseUrl}`, {
    headers: {
      Authorization: `Bearer ${localStorage.getItem("token")}`,
    },
  });
  const data = await res.json();
  return data;
}

export async function GetOnePayment(paymentId) {
  const res = await fetch(`${baseUrl}/${paymentId}`, {
    headers: {
      Authorization: `Bearer ${localStorage.getItem("token")}`,
    },
  });
  const data = await res.json();
  return data;
}

export async function DeletePayment(paymentId) {
  const res = await fetch(`${baseUrl}/${paymentId}`, {
    method: "DELETE",
    headers: {
      Authorization: `Bearer ${localStorage.getItem("token")}`,
    },
  });
  const data = await res.json();
  return data;
}
