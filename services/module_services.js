const baseUrl = "https://hccc-sis.herokuapp.com/api/module";

export async function CreateModule(
  name,
  description,
  referenceId,
  uploadUrl,
  createdBy
) {
  const res = await fetch(`${baseUrl}`, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${localStorage.getItem("token")}`,
    },
    body: JSON.stringify({
      name: name,
      description: description,
      referenceId: referenceId,
      uploadUrl: uploadUrl,
      createdBy: createdBy,
    }),
  });
  const data = await res.json();
  return data;
}
export async function UpdateModule(
  name,
  description,
  referenceId,
  uploadUrl,
  createdBy
) {
  const res = await fetch(`${baseUrl}`, {
    method: "PUT",
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${localStorage.getItem("token")}`,
    },
    body: JSON.stringify({
      name: name,
      description: description,
      referenceId: referenceId,
      uploadUrl: uploadUrl,
      createdBy: createdBy,
    }),
  });
  const data = await res.json();
  return data;
}
export async function GetAllModule() {
  const res = await fetch(`${baseUrl}`, {
    headers: {
      Authorization: `Bearer ${localStorage.getItem("token")}`,
    },
  });
  const data = await res.json();
  return data;
}

export async function PushToCurrentModule(
  submittedUrl,
  fileName,
  studentId,
  moduleId
) {
  const res = await fetch(`${baseUrl}/push-to-module`, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${localStorage.getItem("token")}`,
    },
    body: JSON.stringify({
      submittedUrl: submittedUrl,
      submittedFileName: fileName,
      studentId: studentId,
      moduleId: moduleId,
    }),
  });
  const data = res.json();
  return data;
}

export async function GetOneModule(moduleId) {
  const res = await fetch(`${baseUrl}/${moduleId}`, {
    headers: {
      Authorization: `Bearer ${localStorage.getItem("token")}`,
    },
  });
  const data = await res.json();
  return data;
}

export async function GetAllSameSubject(subjectId) {
  const res = await fetch(`${baseUrl}/subject/${subjectId}`, {
    headers: {
      Authorization: `Bearer ${localStorage.getItem("token")}`,
    },
  });
  const data = await res.json();
  return data;
}

export async function DeleteModule(moduleId) {
  const res = await fetch(`${baseUrl}/${moduleId}`, {
    method: "DELETE",
    headers: {
      Authorization: `Bearer ${localStorage.getItem("token")}`,
    },
  });
  const data = await res.json();
  return data;
}
