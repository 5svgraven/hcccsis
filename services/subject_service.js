const baseUrl = "https://hccc-sis.herokuapp.com/api";

export async function CreateSubject(name, subjectType, userId) {
  const res = await fetch(`${baseUrl}/subject`, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${localStorage.getItem("token")}`,
    },
    body: JSON.stringify({
      name: name,
      subjectType: subjectType,
      createdBy: userId,
    }),
  });
  const data = await res.json();
  return data;
}

export async function AddSubjecToClass(subjectId) {
  const res = await fetch(`${baseUrl}/subject/add-subject`, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${localStorage.getItem("token")}`,
    },
    body: JSON.stringify({
      subjectId: subjectId,
    }),
  });
  const data = await res.json();
  return data;
}

export async function GetAllSubject() {
  const res = await fetch(`${baseUrl}/subject`, {
    headers: {
      Authorization: `Bearer ${localStorage.getItem("token")}`,
    },
  });
  const data = await res.json();
  return data;
}

export async function GetAllCourseSubject() {
  const res = await fetch(`${baseUrl}/subject/course`, {
    headers: {
      Authorization: `Bearer ${localStorage.getItem("token")}`,
    },
  });
  const data = await res.json();
  return data;
}

export async function GetAllClassSubject() {
  const res = await fetch(`${baseUrl}/subject/class`, {
    headers: {
      Authorization: `Bearer ${localStorage.getItem("token")}`,
    },
  });
  const data = await res.json();
  return data;
}

export async function AssignTeacherToSubject(subjectId, teacherId) {
  const res = await fetch(`${baseUrl}/subject/assign-teacher`, {
    method: "PUT",
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${localStorage.getItem("token")}`,
    },
    body: JSON.stringify({
      subjectId: subjectId,
      teacherId: teacherId,
    }),
  });
  const data = await res.json();
  return data;
}

export async function UpdateSubject(
  subjectId,
  name,
  subjectType,
  teacher,
  subjectStart,
  subjectEnd,
  room,
  maxStudent
) {
  const res = await fetch(`${baseUrl}/subject`, {
    method: "PUT",
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${localStorage.getItem("token")}`,
    },
    body: JSON.stringify({
      subjectId: subjectId,
      name: name,
      subjectType: subjectType,
      teacher: teacher,
      subjectStart: subjectStart,
      subjectEnd: subjectEnd,
      room: room,
      maxStudent: maxStudent,
    }),
  });
  const data = await res.json();
  return data;
}

export async function GetOneSubject(subjectId) {
  const res = await fetch(`${baseUrl}/subject/${subjectId}`, {
    headers: {
      Authorization: `Bearer ${localStorage.getItem("token")}`,
    },
  });
  const data = await res.json();
  return data;
}

export async function DeleteSubject(subjectId) {
  const res = await fetch(`${baseUrl}/subject/${subjectId}`, {
    method: "DELETE",
    headers: {
      Authorization: `Bearer ${localStorage.getItem("token")}`,
    },
  });
  const data = await res.json();
  return data;
}
