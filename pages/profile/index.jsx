import { useState, useContext, useEffect } from "react";
import { Button, Spinner } from "react-bootstrap";
import UserContext from "../../context/UserContext";
import { GetOneStudent } from "../../services/student_service";
import { GetAllSubjectStudent } from "../../services/grades_service";

const Profile = () => {
  const [render, setrender] = useState("re-render");
  const [userDTO, setuserDTO] = useState({});
  const [gradesDTO, setgradesDTO] = useState([]);
  const { user } = useContext(UserContext);

  useEffect(() => {
    getDetails();
    getGrades();
  }, [render]);

  async function getDetails() {
    const res = await GetOneStudent(user.id);
    setuserDTO(res);
  }

  async function getGrades() {
    const res = await GetAllSubjectStudent(user.id);
    setgradesDTO(res);
  }

  const viewGrades = gradesDTO.map((grade, index) => {
    return (
      <div key={index}>
        <p>
          <strong>{grade.referenceName}</strong>: {grade.grade}
        </p>
      </div>
    );
  });

  return (
    <>
      <h1>Profile</h1>
      <p>
        Full Name: {userDTO.firstname} {userDTO.lastname}
      </p>
      <p>Address: {userDTO.address}</p>
      <p>Age: {userDTO.age}</p>
      <p>Email: {userDTO.email}</p>
      <p>Status: {user.status} </p>
      <h3>Grades</h3>
      {viewGrades}
    </>
  );
};

export default Profile;
