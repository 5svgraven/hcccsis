import styles from "../styles/Home.module.css";
import HeadDefault from "../layout/head/HeadDefault";
import LoginStudent from "../component/login/student";
import { Image } from "react-bootstrap";

export default function Home() {
  return (
    <div className={styles.container}>
      <HeadDefault title="HCCC | Login" description="It's a School" />
      <div className={styles.image}>
        <Image src="hccc.png" className={styles.hccclogo} roundedCircle />
      </div>
      <LoginStudent />
    </div>
  );
}
