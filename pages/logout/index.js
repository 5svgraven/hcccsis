import { useContext, useEffect } from "react";
import UserContext from "../../context/UserContext";
import Router from "next/router";

const Logout = () => {
  const { unsetUser } = useContext(UserContext);
  useEffect(() => {
    unsetUser();
    Router.push("/");
    return () => {
      unsetUser();
    };
  }, []);
  return <></>;
};

export default Logout;
