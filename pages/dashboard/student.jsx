import { Tab, Row, Col, Nav, Spinner } from "react-bootstrap";
import { useState, useContext, useEffect } from "react";
import HeadDefault from "../../layout/head/HeadDefault";
import StudentEnrollmentView from "../../component/student/student_enrollment_view";
import UserContext from "../../context/UserContext";
import EnrolledClass from "../../component/student/enrolled_class";
import EnrolledCourse from "../../component/student/enrolled_course";
import StudentProfile from "../../component/student/student_profile";
import { GetOneSection } from "../../services/section_service";
import { GetOneCourse } from "../../services/course_service";
import { GetDetails } from "../../services/student_service";

const Student = () => {
  const renders = "rerenders";
  const [isBusy, setBusy] = useState(false);
  const [isEnrolled, setIsEnrolled] = useState(false);
  const [classDetail, setClassDetail] = useState([]);
  const [courseDetail, setCourseDetail] = useState([]);
  const { user } = useContext(UserContext);

  useEffect(() => {
    getUserClassDetails();
    getUserCourseDetails();
  }, [renders]);

  //TODO get user class details
  async function getUserClassDetails() {
    setBusy(true);
    const result = await GetDetails();
    if (result.sectionId !== "") {
      const resultClass = await GetOneSection(result.sectionId);
      setBusy(false);
      setClassDetail(resultClass);
    }
    setBusy(false);
  }

  //TODO get user course details
  async function getUserCourseDetails() {
    setBusy(true);
    const result = await GetDetails();
    if (result.courseId !== "") {
      const resultCourse = await GetOneCourse(result.courseId);
      setBusy(false);
      setCourseDetail(resultCourse);
    }
    setBusy(false);
  }

  return (
    <>
      <HeadDefault title="HCCC | Student Dashboard" />
      {user.status === "Not Enrolled" ? (
        <StudentEnrollmentView />
      ) : user.status === "Pre-Enrolled" ? (
        <>Waiting for your payment confirmation</>
      ) : (
        <>
          <h1 className="text-center">Student's Dashboard</h1>
          <Tab.Container
            id="left-tabs-example"
            defaultActiveKey="enrolledClass"
          >
            <Row>
              <Col sm={2}>
                <Nav variant="pills" className="flex-column">
                  <Nav.Item>
                    <Nav.Link eventKey="enrolledClass">Enrolled Class</Nav.Link>
                  </Nav.Item>
                  <Nav.Item>
                    <Nav.Link eventKey="enrolledCourse">
                      Enrolled Course
                    </Nav.Link>
                  </Nav.Item>
                </Nav>
              </Col>
              <Col sm={10}>
                <Tab.Content>
                  <Tab.Pane eventKey="enrolledClass">
                    <EnrolledClass classDetail={classDetail} />
                  </Tab.Pane>
                  <Tab.Pane eventKey="enrolledCourse">
                    <EnrolledCourse courseDetail={courseDetail} />
                  </Tab.Pane>
                </Tab.Content>
              </Col>
            </Row>
          </Tab.Container>
        </>
      )}
    </>
  );
};

export default Student;
