import { useState, useEffect } from "react";
import { Button } from "react-bootstrap";
import StudentClassComponent from "../../../component/student/student_class_view";
import {
  GetAllSection,
  EnrollStudentToClass,
} from "../../../services/section_service";
import HeadDefault from "../../../layout/head/HeadDefault";
import swal from "sweetalert";
import Router from "next/router";

const StudentClassView = () => {
  const [data, setData] = useState([]);

  useEffect(() => {
    getAllSection();
  }, []);

  async function getAllSection() {
    setData(await GetAllSection());
  }

  const dataClassTable = data.map((section) => {
    return (
      <tr key={section._id}>
        <td>{section.name}</td>
        <td>{section.students.length}</td>
        <td>{section.maxStudent}</td>
        <td>
          <Button
            variant="info"
            onClick={() => enrollStudentToClass(section._id, section.name)}
          >
            Enroll
          </Button>
        </td>
      </tr>
    );
  });

  async function enrollStudentToClass(sectionId, sectionName) {
    const willDelete = await swal({
      title: `MUST READ`,
      text:
        "Please wait for the confirmation of your total payment that will be sent in your registerd Email. Thank you",
      icon: "warning",
      buttons: ["Oh noez!", "Oh Noted!"],
    });
    if (willDelete) {
      const result = await EnrollStudentToClass(sectionId);
      if (result) {
        swal(`Thank you, See you soon in our class!`, {
          icon: "success",
        });
      } else {
        swal("Opss! Think again?", {
          icon: "error",
        });
      }
    }
    Router.push("/logout");
  }

  return (
    <>
      <HeadDefault title="HCCC | Choose Class" />
      <StudentClassComponent dataClassTable={dataClassTable} />
    </>
  );
};

export default StudentClassView;
