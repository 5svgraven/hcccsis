import { useState, useEffect } from "react";
import { Button } from "react-bootstrap";
import StudentCourseComponent from "../../../component/student/student_course_view";
import {
  GetAllCourse,
  EnrollStudentToCourse,
} from "../../../services/course_service";
import HeadDefault from "../../../layout/head/HeadDefault";
import swal from "sweetalert";
import Router from "next/router";

const StudentCourseView = () => {
  const [data, setData] = useState([]);

  useEffect(() => {
    getAllCourse();
  }, []);

  async function getAllCourse() {
    setData(await GetAllCourse());
  }

  const dataCourseTable = data.map((course) => {
    return (
      <tr key={course._id}>
        <td>{course.name}</td>
        <td>{course.students.length}</td>
        <td>
          <Button
            variant="info"
            onClick={() => enrollStudentToCourse(course._id, course.name)}
          >
            Enroll
          </Button>
        </td>
      </tr>
    );
  });

  async function enrollStudentToCourse(courseId, courseName) {
    const willDelete = await swal({
      title: `MUST READ`,
      text:
        "Please wait for the confirmation of your total payment that will be sent in your registerd Email. Thank you",
      icon: "warning",
      buttons: ["Oh noez!", "Oh Noted!"],
    });
    if (willDelete) {
      const result = await EnrollStudentToCourse(courseId);
      if (result) {
        swal(`Thank you, See you soon in our class!`, {
          icon: "success",
        });
      } else {
        swal("Opss! Think again?", {
          icon: "error",
        });
      }
    }
    Router.push("/logout");
  }
  return (
    <>
      <HeadDefault title="HCCC | Choose Course" />
      <StudentCourseComponent dataCourseTable={dataCourseTable} />
    </>
  );
};

export default StudentCourseView;
