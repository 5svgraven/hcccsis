import { Tab, Row, Col, Nav } from "react-bootstrap";
import { useState, useContext, useEffect } from "react";
import AssignedSubject from "../../component/teacher/assigned_class";
import Grades from "../../component/teacher/grades";

const Teacher = () => {
  return (
    <>
      <h1 className="text-center">Dashboard Teacher</h1>
      <Tab.Container id="left-tabs-example" defaultActiveKey="assignedSubjects">
        <Row>
          <Col sm={2}>
            <Nav variant="pills" className="flex-column">
              <Nav.Item>
                <Nav.Link eventKey="assignedSubjects">
                  Assigned Subjects
                </Nav.Link>
                <Nav.Link eventKey="grades">Grades</Nav.Link>
              </Nav.Item>
            </Nav>
          </Col>
          <Col sm={10}>
            <Tab.Content>
              <Tab.Pane eventKey="assignedSubjects">
                <AssignedSubject />
              </Tab.Pane>
              <Tab.Pane eventKey="grades">
                <Grades />
              </Tab.Pane>
            </Tab.Content>
          </Col>
        </Row>
      </Tab.Container>
    </>
  );
};

export default Teacher;
