import { Tabs, Tab } from "react-bootstrap";
import { useState, useContext } from "react";
import Student from "../../component/admin/student";
import Teacher from "../../component/admin/teacher";
import Subject from "../../component/admin/subject";
import Section from "../../component/admin/section";
import Payment from "../../component/admin/payment";
import Course from "../../component/admin/course";
import Report from "../../component/admin/report";
import AdminView from "../../component/admin/admin_view";
import HeadDefault from "../../layout/head/HeadDefault";
import UserContext from "../../context/UserContext";

const Admin = () => {
  const [key, setKey] = useState("student");
  const { user } = useContext(UserContext);

  return (
    <>
      <HeadDefault title="HCCC | Dashboard" />
      <h1 className="text-center">Dashboard {user.status}</h1>
      <Tabs
        id="adminDashboardId"
        variant="pills"
        activeKey={key}
        onSelect={(k) => setKey(k)}
        transition={false}
      >
        {user.status === "Admin" ? (
          <Tab eventKey="admin" title="Admin">
            <AdminView />
          </Tab>
        ) : (
          <div></div>
        )}
        {user.status === "Registrar" ? (
          <Tab eventKey="student" title="Student">
            <Student />
          </Tab>
        ) : (
          <div></div>
        )}
        {user.status === "Admin" ? (
          <Tab eventKey="teacher" title="Teacher">
            <Teacher />
          </Tab>
        ) : (
          <div></div>
        )}

        {user.status === "Admin" ? (
          <Tab eventKey="subject" title="Subject">
            <Subject />
          </Tab>
        ) : (
          <div></div>
        )}

        {user.status === "Admin" ? (
          <Tab eventKey="section" title="Section">
            <Section />
          </Tab>
        ) : (
          <div></div>
        )}

        {user.status === "Admin" ? (
          <Tab eventKey="course" title="Course">
            <Course />
          </Tab>
        ) : (
          <div></div>
        )}

        {user.status === "Cashier" ? (
          <Tab eventKey="payment" title="Payment">
            <Payment />
          </Tab>
        ) : (
          <div></div>
        )}
        {user.status === "Admin" ? (
          <Tab eventKey="report" title="Report">
            <Report />
          </Tab>
        ) : (
          <div></div>
        )}
      </Tabs>
    </>
  );
};

export default Admin;
