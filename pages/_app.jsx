import "../styles/globals.css";
import "bootstrap/dist/css/bootstrap.min.css";
import { Container } from "react-bootstrap";
import { useState, useEffect, useRef } from "react";
import { UserProvider } from "../context/UserContext";
import NavBar from "../component/navbar/NavBar";
import Router from "next/router";
import HeadDefault from "../layout/head/HeadDefault";

export default function Index({ Component, pageProps }) {
  const baseUrl = "https://hccc-sis.herokuapp.com/api";
  const studentStatus = useRef();
  const [user, setUser] = useState({
    username: null,
    fullName: null,
    id: null,
    status: null,
  });

  const unsetUser = () => {
    localStorage.clear();
    setUser({ username: null, id: null, fullName: null });
  };

  useEffect(() => {
    const user = localStorage.getItem("typeOfUser");
    if (user === "teacher") {
      fetch(`${baseUrl}/teacher/details`, {
        headers: {
          Authorization: `Bearer ${localStorage.getItem("token")}`,
        },
      })
        .then((res) => res.json())
        .then((data) => {
          setUser({
            username: data.username,
            fullName: data.firstname + " " + data.lastname,
            id: data._id,
          });
          Router.push("/dashboard/teacher");
        });
    } else if (user === "student") {
      fetch(`${baseUrl}/student/details`, {
        headers: {
          Authorization: `Bearer ${localStorage.getItem("token")}`,
        },
      })
        .then((res) => res.json())
        .then((data) => {
          setUser({
            username: data.username,
            fullName: data.firstname + " " + data.lastname,
            id: data._id,
            status: data.status,
          });
          Router.push("/dashboard/student");
          studentStatus.current = data.status;
        });
    } else if (user === "admin") {
      fetch(`${baseUrl}/admin/details`, {
        headers: {
          Authorization: `Bearer ${localStorage.getItem("token")}`,
        },
      })
        .then((res) => res.json())
        .then((data) => {
          if (data.auth === "failed") {
            setUser({ id: null });
          } else {
            setUser({
              username: data.username,
              fullName: data.firstname + " " + data.lastname,
              id: data._id,
              status: data.role,
            });
            Router.push("/dashboard/admin");
          }
        });
    }
  }, [baseUrl]);

  return (
    <>
      <UserProvider value={{ user, setUser, unsetUser, studentStatus }}>
        <HeadDefault title="HCCC" />
        <Container fluid>
          {user.id === null || user.id === "" ? <div></div> : <NavBar />}

          <Component {...pageProps} />
        </Container>
      </UserProvider>
    </>
  );
}
