import styles from "../../styles/Enrollment.module.css";
import TextField from "../../component/textfield/hccc_textfield";
import { Row, Col, Form, Button } from "react-bootstrap";
import { useState } from "react";
import Link from "next/link";
import HeadDefault from "../../layout/head/HeadDefault";

const Enrollment = () => {
  const [name, setName] = useState("");
  return (
    <>
      <HeadDefault title="HCCC | New Enrollment" description="It's a School" />
      <h1>Enrollment</h1>
      <Row>
        <Col md={4}>
          <TextField
            type="text"
            textValue={name}
            onChange={(e) => setName(e.target.value)}
            placeholder="First Name"
            controlId="firstnameId"
          />
          <TextField
            type="text"
            textValue={name}
            onChange={(e) => setName(e.target.value)}
            placeholder="Last Name"
            controlId="lastnameId"
          />
          <TextField
            type="text"
            textValue={name}
            onChange={(e) => setName(e.target.value)}
            placeholder="Age"
            controlId="ageId"
          />
          <Form.Control as="select" className="selectControl mb-3" bsPrefix>
            <option>Male</option>
            <option>Female</option>
          </Form.Control>
          <TextField
            type="text"
            textValue={name}
            onChange={(e) => setName(e.target.value)}
            placeholder="Address"
            controlId="addressId"
          />
          <TextField
            type="text"
            textValue={name}
            onChange={(e) => setName(e.target.value)}
            placeholder="Mobile No"
            controlId="mobileNoId"
          />
          <TextField
            type="text"
            textValue={name}
            onChange={(e) => setName(e.target.value)}
            placeholder="Parents Name"
            controlId="nameId"
          />
          <TextField
            type="text"
            textValue={name}
            onChange={(e) => setName(e.target.value)}
            placeholder="Parent Mobile No"
            controlId="mobileNoId"
          />
          <TextField
            type="text"
            textValue={name}
            onChange={(e) => setName(e.target.value)}
            placeholder="Username"
            controlId="mobileNoId"
          />
          <TextField
            type="password"
            textValue={name}
            onChange={(e) => setName(e.target.value)}
            placeholder="Password"
            controlId="mobileNoId"
          />
          <div className="d-flex justify-content-start">
            <Button variant="success">Register</Button>
            <Link href="/">
              <Button variant="danger" className="ml-3">
                Cancel
              </Button>
            </Link>
          </div>
        </Col>
      </Row>
    </>
  );
};

export default Enrollment;
