import firebase from "firebase/app";
import "firebase/storage";

var firebaseConfig = {
  apiKey: "AIzaSyDSdNpB4L8wArx0I10ZLb0DyseWYyTEgvs",
  authDomain: "hccc-717fd.firebaseapp.com",
  projectId: "hccc-717fd",
  storageBucket: "hccc-717fd.appspot.com",
  messagingSenderId: "646543043419",
  appId: "1:646543043419:web:16397d8ac122996485887e",
};

!firebase.apps.length ? firebase.initializeApp(firebaseConfig) : firebase.app();

var storage = firebase.storage();

export { storage, firebase as default };
