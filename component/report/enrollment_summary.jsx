import React from "react";
import ReactToPrint from "react-to-print";
import styles from "./styles.module.css";
import { Table } from "react-bootstrap";

class EnrollmentSummary extends React.Component {
  render() {
    return (
      <div className={styles.wrapper}>
        <h1 className="text-center">Enrollment Summary</h1>
        <Table striped bordered hover>
          <thead>
            <tr>
              <th className="text-center">Total Students Enrolled</th>
              <th className="text-center">Total Enrolled in Class</th>
              <th className="text-center">Total Enrolled in Course</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>{this.props.totalStudent}</td>
              <td>{this.props.classStudent}</td>
              <td>{this.props.courseStudent}</td>
            </tr>
          </tbody>
        </Table>
      </div>
    );
  }
}

export default EnrollmentSummary;
