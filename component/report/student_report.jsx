import React from "react";
import styles from "./styles.module.css";
import { Table, Button } from "react-bootstrap";

class StudentReport extends React.Component {
  render() {
    const data = this.props.studentInfo;

    return (
      <div className={styles.wrapper}>
        <h1 className="text-center">Students Report</h1>
        <p>Name: {data.firstname + " " + data.lastname}</p>
        <p>Age: {data.age}</p>
        <p>Gender: {data.gender}</p>
        <p>Mobile No: {data.mobileNo}</p>
        <p>Email: {data.email}</p>
        <p>Role: {data.role}</p>
        <p>Status: {data.status}</p>
      </div>
    );
  }
}

export default StudentReport;
