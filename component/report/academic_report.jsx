import React from "react";
import ReactToPrint from "react-to-print";
import styles from "./styles.module.css";
import { Table } from "react-bootstrap";

class AcademicReport extends React.Component {
  render() {
    const data = this.props.arData;

    const showAcademicTable = data.map((grade) => {
      return (
        <tr key={grade._id}>
          <td>{grade.referenceName}</td>
          <td>{grade.grade}</td>
          <td>{checkGrade(grade.grade)}</td>
        </tr>
      );
    });

    function checkGrade(grade) {
      if (grade > 3.0 && grade < 75) {
        return "Fail";
      } else {
        return "Pass";
      }
    }
    return (
      <div className={styles.wrapper}>
        <h1 className="text-center">Academic Report</h1>
        <Table striped bordered hover>
          <thead>
            <tr>
              <th className="text-center">Name</th>
              <th className="text-center">Grade</th>
              <th className="text-center">Result</th>
            </tr>
          </thead>
          <tbody>{showAcademicTable}</tbody>
        </Table>
      </div>
    );
  }
}

export default AcademicReport;
