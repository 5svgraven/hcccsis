import React from "react";
import style from "./styles.module.css";

export class LoadSlip extends React.Component {
  render() {
    let data = null;
    let showSubjects = null;
    data = this.props.listOfSubjectData;
    try {
      showSubjects = data.map((subject, index) => {
        return (
          <div className={style.tablerow} key={index}>
            <div className={style.tablecell}>{subject.name}</div>
            <div className={style.tablecell}>{subject.teacher}</div>
            <div className={style.tablecell}>{subject.startAt}</div>
            <div className={style.tablecell}>{subject.endAt}</div>
            <div className={style.tablecell}>{subject.room}</div>
          </div>
        );
      });
    } catch (e) {}

    return (
      <div>
        <br></br>
        <br></br>
        <h1 className="text-center">Load Slip</h1>
        <br></br>
        <div className={style.table}>
          <div className={style.tablerow}>
            <div className={style.tablehead}>Name</div>
            <div className={style.tablehead}>Teacher</div>
            <div className={style.tablehead}>Start Time</div>
            <div className={style.tablehead}>End Time</div>
            <div className={style.tablehead}>Room</div>
          </div>
          {showSubjects}
        </div>
      </div>
    );
  }
}
