import Loader from "react-loader-spinner";

const Loading = () => {
  return (
    <div className="loading">
      <div className="innerLoading">
        <Loader type="Circles" color="rgb(255,252,0)" height={50} width={50} />
      </div>
    </div>
  );
};

export default Loading;
