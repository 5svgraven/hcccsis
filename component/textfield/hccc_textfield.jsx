import styles from "./hccc.module.css";
import { Form } from "react-bootstrap";

const TextField = (props) => {
  return (
    <Form.Group controlId={props.controlId} className={styles.formGroup}>
      <p className="ml-1">{props.label}</p>
      <Form.Control
        className={styles.control}
        type={props.type}
        value={props.textValue}
        placeholder={props.placeholder}
        onChange={props.onChange}
      />
    </Form.Group>
  );
};

export default TextField;
