import { Button, Table, Form, Row, Col, Modal, Card } from "react-bootstrap";
import { useState, useContext, useRef } from "react";
import Router from "next/router";
import styles from "../../styles/Login.module.css";
import Link from "next/link";
import TextField from "../../component/textfield/hccc_textfield";
import {
  CreateStudent,
  LoginStudent,
  GetDetails,
  CheckUser,
} from "../../services/student_service";
import {
  LoginTeacher,
  GetDetailsTeacher,
} from "../../services/teacher_service";
import { GetDetailsAdmin } from "../../services/admin_service";
import { loginAdmin } from "../../services/auth_service";
import UserContext from "../../context/UserContext";
import Loading from "../../component/loading";
import swal from "sweetalert";
import { storage } from "../../firebase/index";

const Student = () => {
  const [showCreateModal, setShowCreateModal] = useState(false);
  const { user, setUser, studentStatus } = useContext(UserContext);
  const passwordRef = useRef();
  const [isBusy, setBusy] = useState(false);

  //data student
  const [firstname, setfirstname] = useState("");
  const [lastname, setlastname] = useState("");
  const [age, setage] = useState("");
  const [birthday, setbirthday] = useState("");
  const [gender, setgender] = useState("");
  const [email, setemail] = useState("");
  const [address, setaddress] = useState("");
  const [mobileNo, setmobileNo] = useState("");
  const [parentsName, setparentsName] = useState("");
  const [parentsMobileNo, setparentsMobileNo] = useState("");
  const [studentId, setStudentId] = useState("");
  const [username, setusername] = useState("");
  const [password, setpassword] = useState("");

  const [uploadReportCard, setuploadReportCard] = useState(null);
  const [uploadPicture, setuploadPicture] = useState(null);
  const [uploadGoodMoral, setuploadGoodMoral] = useState(null);
  const [uploadForm, setuploadForm] = useState(null);
  let listOfRequiremnts = [];

  const handleChangeCard = (e) => {
    if (e.target.files[0]) {
      setuploadReportCard(e.target.files[0]);
    }
  };

  const handleChangePicture = (e) => {
    if (e.target.files[0]) {
      setuploadPicture(e.target.files[0]);
    }
  };

  const handleChangeMoral = (e) => {
    if (e.target.files[0]) {
      setuploadGoodMoral(e.target.files[0]);
    }
  };

  const handleChangeForm = (e) => {
    if (e.target.files[0]) {
      setuploadForm(e.target.files[0]);
    }
  };

  const handleClose = () => {
    setShowCreateModal(false);
    setfirstname("");
    setlastname("");
    setage("");
    setgender("");
    setbirthday("");
    setaddress("");
    setmobileNo("");
    setparentsName("");
    setparentsMobileNo("");
    setStudentId("");
    setusername("");
    setpassword("");
    listOfRequiremnts = [];
  };

  const handleUpload = async (uploadFile, name) => {
    try {
      const uploadTask = storage
        .ref(`files/${uploadFile.name}`)
        .put(uploadFile);
      uploadTask.on(
        "state_changed",
        (snapshot) => {},
        (error) => {
          console.log(error);
        },
        async () => {
          await storage
            .ref("files")
            .child(uploadFile.name)
            .getDownloadURL()
            .then((url) => {
              listOfRequiremnts.push({
                name: name,
                link: url,
              });
            });
          if (listOfRequiremnts.length === 4) {
            const result = await CreateStudent(
              firstname,
              lastname,
              age,
              gender,
              birthday,
              address,
              email,
              mobileNo,
              parentsName,
              parentsMobileNo,
              username,
              passwordRef.current,
              listOfRequiremnts
            );
            if (result)
              swal(
                "Congratulations! Your Registration has been submitted to our Registrar.",
                {
                  icon: "success",
                }
              );
            setBusy(false);
            handleClose();
          }
        }
      );
    } catch (e) {
      swal("Opss!", "Something went wrong when uploading", "warning");
    }
  };

  //TODO create student
  async function handleCreate() {
    setBusy(true);
    await handleUpload(uploadReportCard, "Report Card");
    await handleUpload(uploadPicture, "Picture");
    await handleUpload(uploadGoodMoral, "Good Moral");
    await handleUpload(uploadForm, "Form-137");
  }

  async function authenticate(e) {
    e.preventDefault();
    setBusy(true);
    const resultUser = await CheckUser(username);
    if (resultUser.user === "Teacher") {
      const result = await LoginTeacher(username, passwordRef.current);
      if (result) {
        localStorage.setItem("token", result.access);
        localStorage.setItem("typeOfUser", "teacher");
        const teacherInfo = await GetDetailsTeacher();
        setBusy(false);
        setUser({
          username: teacherInfo.username,
          fullname: teacherInfo.firstname + " " + teacherInfo.lastname,
          id: teacherInfo._id,
        });
        Router.push("/dashboard/teacher");
      } else {
        setBusy(false);
        swal("Opss!", "Login Failed", "error");
      }
    } else if (resultUser.user === "Student") {
      const result = await LoginStudent(username, passwordRef.current);
      if (result) {
        localStorage.setItem("token", result.access);
        localStorage.setItem("typeOfUser", "student");
        const studentinfo = await GetDetails();
        setBusy(false);
        if (studentinfo.status === "Not Admitted") {
          localStorage.clear();
          swal(
            "Sorry",
            "You are not yet admitted to Holy Cross College of Carigara the Registrar is still reviewing your requirements please check your email. \n\nThank you for your patience \n\n-HCCC",
            "info"
          );
        } else if (studentinfo.status === "Not Paid") {
          swal(
            "Information",
            "The Registrar have reviewed your requirements and you are accpeted to Holy Cross College of Carigara . Please go to school Cashier for the pre-enrollment fee.",
            "info"
          );
        } else {
          setUser({
            username: studentinfo.username,
            fullname: studentinfo.firstname + " " + studentinfo.lastname,
            id: studentinfo._id,
            status: studentinfo.status,
          });
          Router.push("/dashboard/student");
        }
      } else {
        setBusy(false);
        swal("Opss!", "Login Failed", "error");
      }
    } else if (resultUser.user === "Admin") {
      const result = await loginAdmin(username, passwordRef.current);
      if (result) {
        localStorage.setItem("token", result.access);
        localStorage.setItem("typeOfUser", "admin");
        const admininfo = await GetDetailsAdmin();

        setBusy(false);
        setUser({
          username: admininfo.username,
          fullname: admininfo.firstname + " " + admininfo.lastname,
          id: admininfo._id,
          status: admininfo.role,
        });
        Router.push("/dashboard/admin");
      } else {
        setBusy(false);
        swal("Opss!", "Login Failed", "error");
      }
    } else {
      setBusy(false);
      swal("Opss!", "Login Failed", "error");
    }
  }

  if (isBusy) return <Loading />;
  else
    return (
      <>
        <Row className={styles.customrow}>
          <Card className={styles.customcard}>
            <Col>
              <Form
                onSubmit={(e) => authenticate(e)}
                className={styles.formFormatter}
              >
                <Form.Group
                  controlId="usernameIdd"
                  className={styles.formGroup}
                >
                  <Form.Label className={styles.username}>Username</Form.Label>
                  <Form.Control
                    className={styles.formControl}
                    type="text"
                    value={username}
                    onChange={(e) => setusername(e.target.value)}
                    required
                  />
                </Form.Group>
                <Form.Group controlId="passwordId" className={styles.formGroup}>
                  <Form.Label className={styles.password}>Password</Form.Label>
                  <Form.Control
                    className={styles.formControl}
                    type="password"
                    onChange={(e) => (passwordRef.current = e.target.value)}
                    required
                  />
                </Form.Group>
                <div className={styles.buttonformatter}>
                  <Button
                    bsPrefix
                    type="submit"
                    className={styles.custombutton}
                  >
                    Login
                  </Button>
                </div>
                <p className="text-center my-2">
                  Are you a student?<br></br>
                  <a
                    className={styles.customtag}
                    onClick={() => {
                      setShowCreateModal(true);
                    }}
                  >
                    Click here for Pre-enrollment
                  </a>
                </p>
              </Form>
            </Col>
          </Card>
        </Row>
        <Modal
          show={showCreateModal}
          onHide={handleClose}
          centered
          size="lg"
          aria-labelledby="createModal"
        >
          <Modal.Header closeButton>
            <Modal.Title>Pre-Enrollment Form </Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <strong style={{ color: "red" }}>
              PS: MAKE SURE YOU UPLOAD ALL NECCESSARY REQUIREMENTS BELOW
            </strong>
            <br className="mb-5"></br>
            <TextField
              label="First name"
              type="text"
              textValue={firstname}
              onChange={(e) => setfirstname(e.target.value)}
              placeholder="First Name"
              controlId="firstnameId"
            />
            <TextField
              label="Last name"
              type="text"
              textValue={lastname}
              onChange={(e) => setlastname(e.target.value)}
              placeholder="Last Name"
              controlId="lastnameId"
            />
            <TextField
              label="Age"
              type="text"
              textValue={age}
              onChange={(e) => setage(e.target.value)}
              placeholder="Age"
              controlId="ageId"
            />

            <TextField
              label="Birthday"
              type="text"
              textValue={birthday}
              onChange={(e) => setbirthday(e.target.value)}
              placeholder="YYYY-MM-DD use this format"
              controlId="birthdayId"
            />
            <Form.Control
              as="select"
              className="selectControl mb-3"
              onChange={(e) => setgender(e.target.value)}
            >
              <option>Choose Gender</option>
              <option>Male</option>
              <option>Female</option>
            </Form.Control>
            <TextField
              label="address"
              type="text"
              textValue={address}
              onChange={(e) => setaddress(e.target.value)}
              placeholder="Address"
              controlId="addressId"
            />
            <TextField
              label="Email Address"
              type="email"
              textValue={email}
              onChange={(e) => setemail(e.target.value)}
              placeholder="Email"
              controlId="emailId"
            />
            <TextField
              label="Mobile Number"
              type="text"
              textValue={mobileNo}
              onChange={(e) => setmobileNo(e.target.value)}
              placeholder="Mobile No"
              controlId="mobileNoId"
            />
            <TextField
              label="Parent/s Name"
              type="text"
              textValue={parentsName}
              onChange={(e) => setparentsName(e.target.value)}
              placeholder="Parents Name"
              controlId="parentNameId"
            />
            <TextField
              label="Parent/s Mobile Number"
              type="text"
              textValue={parentsMobileNo}
              onChange={(e) => setparentsMobileNo(e.target.value)}
              placeholder="Parents Mobile No"
              controlId="parentMobileId"
            />
            <TextField
              label="Username"
              type="text"
              onChange={(e) => setusername(e.target.value)}
              placeholder="Username"
              controlId="usernameId"
            />
            <TextField
              label="Password"
              type="password"
              onChange={(e) => (passwordRef.current = e.target.value)}
              placholder="Password"
            />
            <h5 style={{ color: "red" }}>Requirements</h5>
            <strong>Report Card </strong>
            <br></br>
            <input type="file" onChange={handleChangeCard} className="mb-3" />
            <br></br>
            <strong>2x2 Picture </strong>
            <br></br>
            <input
              type="file"
              onChange={handleChangePicture}
              className="mb-3"
            />
            <br></br>
            <strong>Good Moral </strong>
            <br></br>
            <input type="file" onChange={handleChangeMoral} className="mb-3" />
            <br></br>
            <strong>Form-137 </strong>
            <br></br>
            <input type="file" onChange={handleChangeForm} className="mb-3" />
          </Modal.Body>
          <Modal.Footer>
            <Button variant="secondary" onClick={() => handleClose()}>
              Close
            </Button>
            <Button variant="primary" onClick={handleCreate}>
              Create Account
            </Button>
          </Modal.Footer>
        </Modal>
      </>
    );
};

export default Student;
