import { useState, useEffect, useContext } from "react";
import {
  Button,
  Card,
  Modal,
  Form,
  Row,
  Col,
  Table,
  Nav,
} from "react-bootstrap";
import styles from "./styles.module.css";
import Loader from "react-loader-spinner";
import { GetDetailsTeacher } from "../../services/teacher_service";
import { GetOneStudent } from "../../services/student_service";
import { GetOneSubject } from "../../services/subject_service";
import UserContext from "../../context/UserContext";
import {
  CreateModule,
  GetAllSameSubject,
  DeleteModule,
  GetAllModule,
} from "../../services/module_services";
import { CreateGrade } from "../../services/grades_service";
import { storage } from "../../firebase/index";
import swal from "sweetalert";
import TextField from "../../component/textfield/hccc_textfield";
import Loading from "../loading";

const AssignedClass = () => {
  const { user } = useContext(UserContext);
  const render = "re-render";
  const [moduleData, setmoduleData] = useState([]);
  const [moduleStudentData, setmoduleStudentData] = useState([]);
  const [showModuleStudentData, setshowModuleStudentData] = useState(false);
  const [showStudentDetail, setshowStudentDetail] = useState(false);
  const [isBusy, setBusy] = useState(false);
  const [studentDetail, setstudentDetail] = useState({
    firstname: null,
    lastname: null,
    email: null,
    mobileNo: null,
  });
  const [showUploadMdal, setshowUploadModal] = useState(false);
  const [showUploadedModal, setshowUploadedModal] = useState(false);
  const [subjectId, setsubjectId] = useState("");
  const [studentId, setstudentId] = useState("");
  const [description, setdescription] = useState("");
  const [uploadFile, setUploadFile] = useState(null);
  const [grade, setgrade] = useState(0);
  const handleClose = () => {
    setshowUploadedModal(false);
    setshowUploadModal(false);
    setsubjectId("");
    setstudentId("");
    setgrade("");
    setdescription("");
    setmoduleData([]);
    setUploadFile(null);
    setstudentDetail({
      firstname: null,
      lastname: null,
      email: null,
      mobileNo: null,
    });
  };
  const [listOfSubjects, setlistOfSubjects] = useState([]);

  const handleChange = (e) => {
    if (e.target.files[0]) {
      setUploadFile(e.target.files[0]);
    }
  };

  const handleUpload = () => {
    try {
      setBusy(true);
      if (uploadFile === null) {
        swal("Please Selecet a File", { icon: "info" });
      } else {
        const uploadTask = storage
          .ref(`files/${uploadFile.name}`)
          .put(uploadFile);
        uploadTask.on(
          "state_changed",
          (snapshot) => {},
          (error) => {
            console.log(error);
          },
          () => {
            storage
              .ref("files")
              .child(uploadFile.name)
              .getDownloadURL()
              .then((url) => {
                createModule(
                  uploadFile.name,
                  description,
                  subjectId,
                  url,
                  user.id
                );
              });
          }
        );
      }
    } catch (e) {}
  };

  //TODO Create Module
  async function createModule(
    name,
    description,
    referenceId,
    uploadUrl,
    createdBy
  ) {
    const result = await CreateModule(
      name,
      description,
      referenceId,
      uploadUrl,
      createdBy
    );
    setBusy(false);
    if (result) {
      swal("Poof! File Uploaded Successfully!", {
        icon: "success",
      });
    }
    handleClose();
  }

  //TODO create grades
  async function createGrade(grade, subjectId, studentId, userId) {
    setBusy(true);
    const result = await CreateGrade(grade, subjectId, studentId, userId);
    setBusy(false);
    if (result) {
      swal("Poof! Grade Added Successfully!", {
        icon: "success",
      });
    } else {
      swal("Poof! Failed Adding Grade!", {
        icon: "error",
      });
    }
  }

  useEffect(() => {
    getDetails();
  }, [render]);

  async function getDetails() {
    setBusy(true);
    const result = await GetDetailsTeacher();
    setBusy(false);
    setlistOfSubjects(result.subjects);
  }

  async function getModuleData(subjectId) {
    setBusy(true);
    const result = await GetAllSameSubject(subjectId);
    setBusy(false);
    setmoduleData(result);
  }

  async function deleteModule(modId) {
    const willDelete = await swal({
      title: "Are you sure?",
      text: "Once deleted, you will not be able to recover this Module!",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    });

    if (willDelete) {
      const result = await DeleteModule(modId);
      if (result) {
        swal("Poof! Module has been deleted!", {
          icon: "info",
        });
        getModuleData(subjectId);
      } else {
        swal("Opss! Module deletion failed!", {
          icon: "error",
        });
        getModuleData(subjectId);
      }
    } else {
      getModuleData(subjectId);
    }
  }

  const moduleDataTable = moduleData.map((module) => {
    return (
      <tr key={module._id}>
        <td>{module.name}</td>
        <td>{module.description}</td>
        <td>
          <Button
            onClick={() => {
              setmoduleStudentData(module.submittedFilesByStudent);
              setshowModuleStudentData(true);
            }}
          >
            View
          </Button>
        </td>
        <td>
          <a href={module.uploadUrl} target="_blank">
            <Button>Link</Button>
          </a>
        </td>
        <td>
          <Button variant="danger" onClick={() => deleteModule(module._id)}>
            Delete
          </Button>
        </td>
      </tr>
    );
  });

  const subjectCardViews = listOfSubjects.map((subject) => {
    return (
      <Card className={styles.cardClass} key={subject._id}>
        <Card.Header className="text-center">
          Holy Cross College of Carigara
        </Card.Header>
        <Card.Body>
          <Card.Title className="text-center">{subject.subjectName}</Card.Title>
        </Card.Body>
        <Card.Footer>
          <div className="d-flex justify-content-between">
            <Button
              variant="warning"
              onClick={() => {
                setsubjectId(subject.subjectId);
                setshowUploadModal(true);
              }}
            >
              Upload File
            </Button>
            <Button
              variant="success"
              onClick={() => {
                setsubjectId(subject.subjectId);
                getModuleData(subject.subjectId);
                setshowUploadedModal(true);
              }}
            >
              View Uploaded Files
            </Button>
          </div>
        </Card.Footer>
      </Card>
    );
  });

  const studentModuleTable = moduleStudentData.map((module) => {
    return (
      <tr key={module._id}>
        <td>{module.submittedFileName}</td>
        <td>
          <a href={module.submittedUrl} target="_blank">
            <Button>Link</Button>
          </a>
        </td>
        <td>
          <Button
            variant="warning"
            onClick={() => {
              getOneStudent(module.studentId);
              setstudentId(module.studentId);
              setshowStudentDetail(true);
            }}
          >
            View Student
          </Button>
        </td>
      </tr>
    );
  });

  async function getOneStudent(studentId) {
    const result = await GetOneStudent(studentId);
    setstudentDetail({
      firstname: result.firstname,
      lastname: result.lastname,
      email: result.email,
      mobileNo: result.mobileNo,
    });
  }

  return (
    <>
      <Row>{subjectCardViews}</Row>

      <Modal
        show={showUploadMdal}
        onHide={handleClose}
        centered
        size="md"
        aria-labelledby="updateModal"
      >
        <Modal.Header closeButton>
          <Modal.Title>Upload a file</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <input type="file" onChange={handleChange} />
          <Form.Control
            className="mt-2"
            as="textarea"
            placeholder="Description"
            onChange={(e) => setdescription(e.target.value)}
          />
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleClose}>
            Close
          </Button>
          <Button
            variant="primary"
            onClick={() => {
              handleUpload();
            }}
            disabled={description === "" ? true : false}
          >
            Upload File
          </Button>
        </Modal.Footer>
      </Modal>
      <Modal
        show={showUploadedModal}
        onHide={handleClose}
        centered
        size="xl"
        aria-labelledby="updateModal"
      >
        <Modal.Header closeButton>
          <Modal.Title>Teacher Uploaded File Detail</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <h5>Teacher's Upload</h5>
          <Table striped bordered hover>
            <thead>
              <tr>
                <th>File Name</th>
                <th>Description</th>
                <th>View Details</th>
                <th>Download Link</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>{moduleDataTable}</tbody>
          </Table>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleClose}>
            Close
          </Button>
        </Modal.Footer>
      </Modal>
      <Modal
        show={showModuleStudentData}
        onHide={() => setshowModuleStudentData(false)}
        centered
        size="xl"
        aria-labelledby="updateModal"
      >
        <Modal.Header closeButton>
          <Modal.Title>Student Uploaded File Detail</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <h5>Student Upload</h5>
          <Table striped bordered hover>
            <thead>
              <tr>
                <th>File Name</th>
                <th>Download Link</th>
                <th>Student Information</th>
              </tr>
            </thead>
            <tbody>{studentModuleTable}</tbody>
          </Table>
        </Modal.Body>
        <Modal.Footer>
          <Button
            variant="secondary"
            onClick={() => setshowModuleStudentData(false)}
          >
            Close
          </Button>
        </Modal.Footer>
      </Modal>
      <Modal
        show={showStudentDetail}
        onHide={() => {
          setshowStudentDetail(false);
        }}
        centered
        size="md"
        aria-labelledby="updateModal"
      >
        <Modal.Header closeButton>
          <Modal.Title>Student Details and Grade</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <p>
            <strong>First Name:</strong> {studentDetail.firstname}
          </p>
          <p>
            <strong>Last Name:</strong> {studentDetail.lastname}
          </p>
          <p>
            <strong>Email:</strong> {studentDetail.email}
          </p>
          <p>
            <strong>Mobile No.:</strong> {studentDetail.mobileNo}
          </p>
          <TextField
            label="Grade"
            type="Number"
            textValue={grade}
            onChange={(e) => setgrade(e.target.value)}
            placeholder="Grade"
            controlId="gradeId"
          />
        </Modal.Body>
        <Modal.Footer>
          <Button
            variant="secondary"
            onClick={() => setshowStudentDetail(false)}
          >
            Close
          </Button>
          <Button
            variant="info"
            onClick={() => {
              //TODO Add to Grade
              createGrade(grade, subjectId, studentId, user.id);
              setshowStudentDetail(false);
            }}
          >
            Submit Grade
          </Button>
        </Modal.Footer>
      </Modal>
      {isBusy ? <Loading /> : <div></div>}
    </>
  );
};

export default AssignedClass;
