import { useState, useEffect, useContext } from "react";
import {
  Button,
  Card,
  Modal,
  Form,
  Row,
  Col,
  Table,
  Nav,
} from "react-bootstrap";
import swal from "sweetalert";
import UserContext from "../../context/UserContext";
import {
  CreateGrade,
  UpdateGrade,
  GetAllGrade,
  GetOneGrade,
  DeleteGrade,
  GetAllSubjectGrades,
} from "../../services/grades_service";
import { GetDetailsTeacher } from "../../services/teacher_service";
import { GetOneStudent } from "../../services/student_service";
import styles from "./styles.module.css";
import TextField from "../../component/textfield/hccc_textfield";

const Grades = () => {
  const render = "re-render";
  const [listOfSubjects, setlistOfSubjects] = useState([]);
  const [listOfGrades, setlistOfGrades] = useState([]);
  const [subjectId, setsubjectId] = useState("");
  const [gradeId, setgradeId] = useState();
  const [gradeNo, setgradeNo] = useState();
  const [studentDetail, setstudentDetail] = useState({
    firstname: null,
    lastname: null,
    email: null,
    mobileNo: null,
  });
  const [showListOfGrades, setshowListOfGrades] = useState(false);
  const [showStudentDetail, setshowStudentDetail] = useState(false);
  const [showGradeUpdate, setshowGradeUpdate] = useState(false);
  const handleClose = () => {
    setshowListOfGrades(false);
  };

  useEffect(() => {
    getDetails();
  }, [render]);

  async function getDetails() {
    const result = await GetDetailsTeacher();
    setlistOfSubjects(result.subjects);
  }

  async function getAllSubjectGrades(subjectId) {
    const result = await GetAllSubjectGrades(subjectId);
    setlistOfGrades(result);
  }

  async function getOneStudent(studentId) {
    const result = await GetOneStudent(studentId);
    setstudentDetail({
      firstname: result.firstname,
      lastname: result.lastname,
      email: result.email,
      mobileNo: result.mobileNo,
    });
  }

  async function updateGrade(gradeId, graded) {
    const result = await UpdateGrade(gradeId, graded);
    getAllSubjectGrades(subjectId);
    swal("Yosh! Grade has been Updated!", {
      icon: "success",
    });
  }

  async function handleDelete(gradeId) {
    const willDelete = await swal({
      title: "Are you sure?",
      text: "Once deleted, you will not be able to recover this Grade!",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    });

    if (willDelete) {
      const result = await DeleteGrade(gradeId);
      if (result) {
        swal("Poof! Grade has been deleted!", {
          icon: "info",
        });
        handleClose();
      } else {
        swal("Opss! Grade deletion failed!", {
          icon: "error",
        });
        handleClose();
      }
    } else {
      handleClose();
    }
    getAllSubjectGrades(subjectId);
  }

  const subjectCardViews = listOfSubjects.map((subject) => {
    return (
      <Card className={styles.cardClass} key={subject._id}>
        <Card.Header className="text-center">
          Holy Cross College of Carigara
        </Card.Header>
        <Card.Body>
          <Card.Title className="text-center">{subject.subjectName}</Card.Title>
        </Card.Body>
        <Card.Footer>
          <div className="d-flex justify-content-between">
            <Button
              variant="success"
              onClick={() => {
                setsubjectId(subject.subjectId);
                getAllSubjectGrades(subject.subjectId);
                setshowListOfGrades(true);
              }}
            >
              View Grades
            </Button>
          </div>
        </Card.Footer>
      </Card>
    );
  });

  const showGradesTable = listOfGrades.map((grade) => {
    return (
      <tr key={grade._id}>
        <td>{grade.grade}</td>
        <td>
          <Button
            variant="info"
            onClick={() => {
              getOneStudent(grade.studentId);
              setshowStudentDetail(true);
            }}
          >
            View Student
          </Button>
        </td>
        <td>
          <div className="d-flex justify-content-center">
            <Button
              onClick={() => {
                setgradeId(grade._id);
                setgradeNo(grade.grade);
                setshowGradeUpdate(true);
              }}
            >
              Update
            </Button>
            <Button
              variant="danger"
              className="ml-2"
              onClick={() => {
                setgradeId(grade._id);
                handleDelete(grade._id);
              }}
            >
              Delete
            </Button>
          </div>
        </td>
      </tr>
    );
  });
  return (
    <>
      <Row>{subjectCardViews}</Row>
      <Modal
        show={showListOfGrades}
        onHide={handleClose}
        centered
        size="xl"
        aria-labelledby="showGradeId"
      >
        <Modal.Header closeButton>
          <Modal.Title>List of Grades</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Table striped bordered hover>
            <thead>
              <tr>
                <th>Grade</th>
                <th>Student</th>
                <th>Update</th>
              </tr>
            </thead>
            <tbody>{showGradesTable}</tbody>
          </Table>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleClose}>
            Close
          </Button>
        </Modal.Footer>
      </Modal>
      <Modal
        show={showStudentDetail}
        onHide={() => {
          setshowStudentDetail(false);
        }}
        centered
        size="md"
        aria-labelledby="updateModal"
      >
        <Modal.Header closeButton>
          <Modal.Title>Student Details and Grade</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <p>
            <strong>First Name:</strong> {studentDetail.firstname}
          </p>
          <p>
            <strong>Last Name:</strong> {studentDetail.lastname}
          </p>
          <p>
            <strong>Email:</strong> {studentDetail.email}
          </p>
          <p>
            <strong>Mobile No.:</strong> {studentDetail.mobileNo}
          </p>
        </Modal.Body>
        <Modal.Footer>
          <Button
            variant="secondary"
            onClick={() => setshowStudentDetail(false)}
          >
            Close
          </Button>
        </Modal.Footer>
      </Modal>
      <Modal
        show={showGradeUpdate}
        onHide={() => {
          setshowGradeUpdate(false);
        }}
        centered
        size="md"
        aria-labelledby="updateModal"
      >
        <Modal.Header closeButton>
          <Modal.Title>Student Details and Grade</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <TextField
            type="text"
            textValue={gradeNo}
            onChange={(e) => setgradeNo(e.target.value)}
            placeholder="Grade"
            controlId="gradeId"
          />
        </Modal.Body>
        <Modal.Footer>
          <Button
            variant="secondary"
            onClick={() => setshowStudentDetail(false)}
          >
            Close
          </Button>
          <Button
            variant="info"
            onClick={() => {
              updateGrade(gradeId, gradeNo);
              setshowGradeUpdate(false);
            }}
          >
            Update Grade
          </Button>
        </Modal.Footer>
      </Modal>
    </>
  );
};

export default Grades;
