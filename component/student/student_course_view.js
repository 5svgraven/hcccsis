import { useState } from "react";
import { Button, Table } from "react-bootstrap";

const StudentCourseView = (props) => {
  return (
    <>
      <h1>Choose a Course to enroll</h1>
      <Table striped bordered hover>
        <thead>
          <tr>
            <th>Name</th>
            <th>No. of Student</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>{props.dataCourseTable}</tbody>
      </Table>
    </>
  );
};

export default StudentCourseView;
