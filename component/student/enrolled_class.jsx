import { useState, useEffect, useContext, useRef } from "react";
import {
  Button,
  Card,
  Modal,
  Form,
  Row,
  Col,
  Tab,
  Table,
  Nav,
} from "react-bootstrap";
import styles from "./styles.module.css";
import {
  GetAllSameSubject,
  PushToCurrentModule,
} from "../../services/module_services";
import { GetOneSubject } from "../../services/subject_service";
import UserContext from "../../context/UserContext";
import { storage } from "../../firebase/index";
import swal from "sweetalert";
import { LoadSlip } from "../report/load_slip";
import { useReactToPrint } from "react-to-print";
import { GetOneTeacher } from "../../services/teacher_service";
import Loading from "../loading";

const EnrolledClass = (props) => {
  const { user } = useContext(UserContext);
  const render = "rerender";
  const [isBusy, setBusy] = useState(false);
  const [moduleData, setmoduleData] = useState([]);
  const [moduleStudentData, setmoduleStudentData] = useState([]);
  const [showModuleStudentData, setshowModuleStudentData] = useState(false);
  const [showUploadMdal, setshowUploadModal] = useState(false);
  const [showUploadedModal, setshowUploadedModal] = useState(false);
  const [showSubjectDetail, setshowSubjectDetail] = useState(false);
  const [viewCourseSubject, setviewCourseSubject] = useState(false);
  const [showClassDetail, setshowClassDetail] = useState(false);
  const [listOfSubjectData, setlistOfSubjectData] = useState(null);
  const [uploadFile, setUploadFile] = useState(null);
  const [subjectId, setsubjectId] = useState("");
  const [moduleId, setmoduleId] = useState("");
  const loadSlipRef = useRef();

  const handleClose = () => {
    setshowSubjectDetail(false);
    setshowClassDetail(false);
    setviewCourseSubject(false);
    setmoduleId("");
    setsubjectId("");
    setUploadFile(null);
    setmoduleData([]);
  };

  const handleClose2 = () => {
    setshowUploadModal(false);
    setshowUploadedModal(false);
  };
  const section = props.classDetail;
  const listOfSubject = props.classDetail.subjects;
  let subjectCardViews = null;
  let subjects = [];

  const handleChange = (e) => {
    if (e.target.files[0]) {
      setUploadFile(e.target.files[0]);
    }
  };

  const handleUpload = () => {
    setBusy(true);
    const uploadTask = storage.ref(`files/${uploadFile.name}`).put(uploadFile);
    uploadTask.on(
      "state_changed",
      (snapshot) => {},
      (error) => {
        console.log(error);
      },
      () => {
        storage
          .ref("files")
          .child(uploadFile.name)
          .getDownloadURL()
          .then((url) => {
            pushToCurrentModule(url, uploadFile.name, user.id, moduleId);
          });
      }
    );
  };

  async function pushToCurrentModule(
    submittedUrl,
    fileName,
    studentId,
    moduleId
  ) {
    const result = await PushToCurrentModule(
      submittedUrl,
      fileName,
      studentId,
      moduleId
    );
    setBusy(false);
    if (result) {
      swal("Poof! File Uploaded Successfully!", {
        icon: "success",
      });
    }
    handleClose();
    handleClose2();
  }

  async function getModuleData(subjectId) {
    setBusy(true);
    const result = await GetAllSameSubject(subjectId);
    setBusy(false);
    setmoduleData(result);
  }

  const moduleDataTable = moduleData.map((module) => {
    return (
      <tr key={module._id}>
        <td>{module.name}</td>
        <td>{module.description}</td>
        <td>
          <a href={module.uploadUrl} target="_blank">
            <Button>Link</Button>
          </a>
        </td>
        <td>
          <Button
            variant="success"
            onClick={() => {
              setmoduleStudentData(module.submittedFilesByStudent);
              setshowModuleStudentData(true);
            }}
          >
            View
          </Button>
        </td>
        <td>
          <Button
            variant="warning"
            onClick={() => {
              setmoduleId(module._id);
              setshowUploadModal(true);
            }}
          >
            Upload File
          </Button>
        </td>
      </tr>
    );
  });

  const handlePrintLoadSlip = useReactToPrint({
    content: () => loadSlipRef.current,
  });

  async function getAllSubjectData() {
    setBusy(true);
    const data = await Promise.all(
      listOfSubject.map(async (subject) => {
        let subjectData = await GetOneSubject(subject.subjectId);
        let teacherData = await GetOneTeacher(subjectData.teacher);
        return {
          name: subjectData.name,
          room: subjectData.room,
          startAt: subjectData.subjectStart,
          endAt: subjectData.subjectEnd,
          teacher: teacherData.firstname + " " + teacherData.lastname,
        };
      })
    );
    setBusy(false);
    setlistOfSubjectData(data);
    handlePrintLoadSlip();
  }

  if (section.length !== 0) {
    subjectCardViews = listOfSubject.map((subject) => {
      return (
        <Card className={styles.cardClass} key={subject._id}>
          <Card.Header className="text-center">
            Holy Cross College of Carigara
          </Card.Header>
          <Card.Body>
            <Card.Title className="text-center">
              {subject.subjectName}
            </Card.Title>
          </Card.Body>
          <Card.Footer>
            <div className="d-flex justify-content-between">
              <Button
                variant="success"
                onClick={() => {
                  setsubjectId(subject.subjectId);
                  getModuleData(subject.subjectId);
                  setshowUploadedModal(true);
                }}
              >
                View Uploaded Files
              </Button>
            </div>
          </Card.Footer>
        </Card>
      );
    });
  }

  const studentModuleTable = moduleStudentData.map((module) => {
    return (
      <tr key={module._id}>
        <td>{module.submittedFileName}</td>
        <td>
          <a href={module.submittedUrl} target="_blank">
            <Button>Link</Button>
          </a>
        </td>
      </tr>
    );
  });

  return (
    <>
      {section.length !== 0 ? (
        <Card className={styles.cardClass}>
          <Card.Header className="text-center">
            Holy Cross College of Carigara
          </Card.Header>
          <Card.Body>
            <Card.Title className="text-center">{section.name}</Card.Title>
          </Card.Body>
          <Card.Footer>
            <Button variant="primary" onClick={() => setshowClassDetail(true)}>
              View Class
            </Button>
          </Card.Footer>
        </Card>
      ) : (
        <div className="d-flex justify-content-center mt-5 ">
          <h3>You are enrolled to course</h3>
        </div>
      )}
      <Modal
        show={showClassDetail}
        onHide={handleClose}
        centered
        size="xl"
        aria-labelledby="viewclassdetail"
      >
        <Modal.Header closeButton>
          <Modal.Title>Class Details</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Tab.Container id="left-tabs-example" defaultActiveKey="classDetail">
            <Row>
              <Col sm={2}>
                <Nav variant="pills" className="flex-column">
                  <Nav.Item>
                    <Nav.Link eventKey="classDetail">Class Subjects</Nav.Link>
                  </Nav.Item>
                  {/* <Nav.Item>
                    <Nav.Link eventKey="subjectTable">Load Slip</Nav.Link>
                  </Nav.Item> */}
                </Nav>
              </Col>
              <Col sm={10}>
                <Tab.Content>
                  <Tab.Pane eventKey="classDetail">
                    <Row>{subjectCardViews}</Row>
                  </Tab.Pane>
                  {/* <Tab.Pane eventKey="subjectTable">
                    <LoadSlip listOfSubject={listOfSubject} />
                  </Tab.Pane> */}
                </Tab.Content>
              </Col>
            </Row>
          </Tab.Container>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="primary" onClick={() => getAllSubjectData()}>
            Print Load Slip
          </Button>
          <Button variant="success" onClick={handleClose}>
            Cancel
          </Button>
        </Modal.Footer>
      </Modal>
      <Modal
        show={showUploadMdal}
        onHide={handleClose2}
        centered
        size="md"
        aria-labelledby="updateModal"
      >
        <Modal.Header closeButton>
          <Modal.Title>Upload a file</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <input type="file" onChange={handleChange} />
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleClose2}>
            Close
          </Button>
          <Button variant="primary" onClick={handleUpload}>
            Upload File
          </Button>
        </Modal.Footer>
      </Modal>
      <Modal
        show={showUploadedModal}
        onHide={handleClose2}
        centered
        size="xl"
        aria-labelledby="updateModal"
      >
        <Modal.Header closeButton>
          <Modal.Title>
            Uploaded File Detail with Students Uploaded Files
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <h5>Teacher's Upload</h5>
          <Table striped bordered hover>
            <thead>
              <tr>
                <th>File Name</th>
                <th>Description</th>
                <th>Download Link</th>
                <th>View Uploaded Files</th>
                <th>Upload Here</th>
              </tr>
            </thead>
            <tbody>{moduleDataTable}</tbody>
          </Table>
        </Modal.Body>
        <Modal.Footer>
          <Button
            variant="secondary"
            onClick={() => setshowUploadedModal(false)}
          >
            Close
          </Button>
        </Modal.Footer>
      </Modal>
      <Modal
        show={showModuleStudentData}
        onHide={() => setshowModuleStudentData(false)}
        centered
        size="xl"
        aria-labelledby="updateModal"
      >
        <Modal.Header closeButton>
          <Modal.Title>Students Uploaded files</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <h5>Student's Upload</h5>
          <Table striped bordered hover>
            <thead>
              <tr>
                <th>File Name</th>
                <th>Download Link</th>
              </tr>
            </thead>
            <tbody>{studentModuleTable}</tbody>
          </Table>
        </Modal.Body>
        <Modal.Footer>
          <Button
            variant="secondary"
            onClick={() => setshowModuleStudentData(false)}
          >
            Close
          </Button>
        </Modal.Footer>
      </Modal>
      <div className="hide">
        <LoadSlip listOfSubjectData={listOfSubjectData} ref={loadSlipRef} />
      </div>
      {isBusy ? <Loading /> : <div></div>}
    </>
  );
};

export default EnrolledClass;
