import { useState } from "react";
import { Button, Table } from "react-bootstrap";

const StudentClassView = (props) => {
  return (
    <>
      <h1>Choose a Class to enroll</h1>
      <Table striped bordered hover>
        <thead>
          <tr>
            <th>Name</th>
            <th>No. of Student</th>
            <th>Max Student</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>{props.dataClassTable}</tbody>
      </Table>
    </>
  );
};

export default StudentClassView;
