import { useState } from "react";
import { Button } from "react-bootstrap";
import { storage } from "../../firebase/index";

const StudentUpload = () => {
  const [uploadFile, setUploadFile] = useState(null);

  const handleChange = (e) => {
    if (e.target.files[0]) {
      setUploadFile(e.target.files[0]);
    }
  };

  const handleUpload = () => {
    const uploadTask = storage.ref(`files/${uploadFile.name}`).put(uploadFile);
    uploadTask.on(
      "state_changed",
      (snapshot) => {},
      (error) => {
        console.log(error);
      },
      () => {
        storage
          .ref("files")
          .child(uploadFile.name)
          .getDownloadURL()
          .then((url) => {
            console.log(url);
          });
      }
    );
  };
  return (
    <>
      <h1>Student Upload</h1>
      <input type="file" onChange={handleChange} />
      <button onClick={handleUpload}>Uplaod</button>
    </>
  );
};

export default StudentUpload;
