import { useState } from "react";
import { Button } from "react-bootstrap";
import styles from "./styles.module.css";
import Link from "next/link";
import HeadDefault from "../../layout/head/HeadDefault";

const StudentEnrollmentView = () => {
  return (
    <>
      <HeadDefault title="HCCC | Choose" />
      <h1 className="text-center">What do you wish to enroll to?</h1>
      <div className={styles.choiceCenter}>
        <Button size="lg" variant="success">
          <Link href="/dashboard/student/student_class_view">
            <a className="anchorTag">CLASS</a>
          </Link>
        </Button>
        <Button size="lg" variant="warning">
          <Link href="/dashboard/student/student_course_view">
            <a className="anchorTag">COURSE</a>
          </Link>
        </Button>
      </div>
    </>
  );
};

export default StudentEnrollmentView;
