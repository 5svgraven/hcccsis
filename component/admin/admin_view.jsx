import { useState, useEffect, useContext } from "react";
import { Button, Table, Form, Row, Col, Modal } from "react-bootstrap";
import {
  CreateAdmin,
  UpdateAdmin,
  GetAllAdmin,
  GetOneAdmin,
  DeleteAdmin,
  GetDetails,
} from "../../services/admin_service";
import TextField from "../../component/textfield/hccc_textfield";
import swal from "sweetalert";
import UserContext from "../../context/UserContext";
import Loading from "../../component/loading";

const AdminView = () => {
  const render = "rerender";
  const { user } = useContext(UserContext);
  const [data, setData] = useState([]);
  const [showCreateModal, setShowCreateModal] = useState(false);
  const [showUpdateModal, setShowUpdateModal] = useState(false);
  const [isBusy, setBusy] = useState(false);
  //Admin Details
  const [adminId, setadminId] = useState();
  const [firstname, setfirstname] = useState("");
  const [lastname, setlastname] = useState("");
  const [age, setage] = useState("");
  const [birthday, setbirthday] = useState("");
  const [gender, setgender] = useState("");
  const [role, setrole] = useState("");
  const [address, setaddress] = useState("");
  const [mobileNo, setmobileNo] = useState("");
  const [username, setusername] = useState("");
  const [password, setpassword] = useState("");
  const [searchText, setsearchText] = useState("");

  const handleClose = () => {
    setShowCreateModal(false);
    setShowUpdateModal(false);
    setfirstname("");
    setlastname("");
    setage("");
    setbirthday("");
    setrole("");
    setgender("");
    setaddress("");
    setmobileNo("");
    setusername("");
    setpassword("");
  };

  //TODO create
  async function handleCreate() {
    setBusy(true);
    const result = await CreateAdmin(
      firstname,
      lastname,
      age,
      gender,
      role,
      birthday,
      address,
      mobileNo,
      username,
      password
    );
    setBusy(false);
    if (result)
      swal("Poof! Admin has been created!", {
        icon: "success",
      });
    getAllAdmin();
    handleClose();
  }
  //TODO handleUpdate
  async function handleUpdate() {
    setBusy(true);
    const result = await UpdateAdmin(
      adminId,
      firstname,
      lastname,
      age,
      gender,
      birthday,
      address,
      mobileNo
    );
    setBusy(false);
    if (result)
      swal("Poof! Admin has been updated!", {
        icon: "success",
      });
    else
      swal("Poof! Failed Update!", {
        icon: "error",
      });
    getAllAdmin();
    handleClose();
  }

  //TODO Get All Admin
  async function getAllAdmin() {
    const result = await GetAllAdmin();
    setData(result);
  }
  //TODO Get One Admin
  async function getOneAdmin(adminId) {
    const result = await GetOneAdmin(adminId);
    setfirstname(result.firstname);
    setlastname(result.lastname);
    setage(result.age);
    setbirthday(result.birthday);
    setaddress(result.address);
    setmobileNo(result.mobileNo);
  }
  //TODO Delete Admin
  async function deleteOneAdmin(adminId) {
    const willDelete = await swal({
      title: "Are you sure?",
      text: "Once deleted, you will not be able to recover this Admin",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    });

    if (willDelete) {
      setBusy(true);
      const result = await DeleteAdmin(adminId);
      setBusy(false);
      if (result) {
        swal("Poof! Admin has been deleted!", {
          icon: "success",
        });
        handleClose();
        getAllAdmin();
      } else {
        swal("Opss! Admin deletion failed!", {
          icon: "error",
        });
        handleClose();
      }
    } else {
    }
    getAllAdmin();
    handleClose();
  }

  useEffect(() => {
    getAllAdmin();
  }, [render]);

  const filteredAdmin = data.filter((admin) => {
    if (searchText === "") {
      return admin;
    } else {
      return (
        admin.firstname.toLowerCase().indexOf(searchText.toLowerCase()) !== -1
      );
    }
  });

  let showAdminTable = filteredAdmin.map((admin) => {
    if (!admin.superAdmin) {
      return (
        <tr key={admin._id}>
          <td>{admin.firstname + " " + admin.lastname}</td>
          <td>{admin.age}</td>
          <td>{admin.gender}</td>
          <td>{admin.mobileNo}</td>
          <td>
            <div className="d-flex justify-content-center">
              <Button
                variant="warning"
                onClick={() => {
                  getOneAdmin(admin._id);
                  setadminId(admin._id);
                  setShowUpdateModal(true);
                }}
              >
                Update
              </Button>
              <Button
                className="ml-2"
                variant="danger"
                onClick={() => {
                  deleteOneAdmin(admin._id);
                }}
              >
                Delete
              </Button>
            </div>
          </td>
        </tr>
      );
    }
  });

  if (isBusy) return <Loading />;
  else
    return (
      <>
        <h1>Admin</h1>
        <Row className="justify-content-center">
          <Col md={10}>
            <Row className="d-flex justify-content-between">
              <Button className="my-2" onClick={() => setShowCreateModal(true)}>
                Add New Admin
              </Button>
              <TextField
                type="text"
                textValue={searchText}
                onChange={(e) => setsearchText(e.target.value)}
                placeholder="Search Admin"
                controlId="SearchId7"
              />
            </Row>
            <Table striped bordered hover>
              <thead>
                <tr>
                  <th>Name</th>
                  <th>Age</th>
                  <th>Gender</th>
                  <th>Mobile No.</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>{showAdminTable}</tbody>
            </Table>
          </Col>
        </Row>
        <Modal
          show={showUpdateModal}
          onHide={handleClose}
          centered
          size="lg"
          aria-labelledby="updateModal"
        >
          <Modal.Header closeButton>
            <Modal.Title>Update Admin</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <TextField
              label="First Name"
              type="text"
              textValue={firstname}
              onChange={(e) => setfirstname(e.target.value)}
              placeholder="First Name"
              controlId="firstnameId"
            />
            <TextField
              label="Last Name"
              type="text"
              textValue={lastname}
              onChange={(e) => setlastname(e.target.value)}
              placeholder="Last Name"
              controlId="lastnameId"
            />
            <TextField
              label="Age"
              type="text"
              textValue={age}
              onChange={(e) => setage(e.target.value)}
              placeholder="Age"
              controlId="ageId"
            />
            <TextField
              label="Birthday"
              type="text"
              textValue={birthday}
              onChange={(e) => setbirthday(e.target.value)}
              placeholder="YYYY-MM-DD use this format"
              controlId="birthdayId"
            />
            <TextField
              label="Address"
              type="text"
              textValue={address}
              onChange={(e) => setaddress(e.target.value)}
              placeholder="Address"
              controlId="addressId"
            />
            <TextField
              label="Mobile No"
              type="text"
              textValue={mobileNo}
              onChange={(e) => setmobileNo(e.target.value)}
              placeholder="Mobile No"
              controlId="mobileNoid"
            />
          </Modal.Body>
          <Modal.Footer>
            <Button variant="secondary" onClick={handleClose}>
              Close
            </Button>
            <Button variant="primary" onClick={handleUpdate}>
              Update Admin
            </Button>
          </Modal.Footer>
        </Modal>
        <Modal
          show={showCreateModal}
          onHide={handleClose}
          centered
          size="lg"
          aria-labelledby="createModal"
        >
          <Modal.Header closeButton>
            <Modal.Title>Create Admin</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <TextField
              label="First name"
              type="text"
              textValue={firstname}
              onChange={(e) => setfirstname(e.target.value)}
              placeholder="First Name"
              controlId="firstnameId"
            />
            <TextField
              label="Last name"
              type="text"
              textValue={lastname}
              onChange={(e) => setlastname(e.target.value)}
              placeholder="Last Name"
              controlId="lastnameId"
            />
            <TextField
              label="Age"
              type="text"
              textValue={age}
              onChange={(e) => setage(e.target.value)}
              placeholder="Age"
              controlId="ageId"
            />

            <TextField
              label="Birthday"
              type="text"
              textValue={birthday}
              onChange={(e) => setbirthday(e.target.value)}
              placeholder="YYYY-MM-DD use this format"
              controlId="birthdayId"
            />
            <Form.Control
              as="select"
              className="selectControl mb-3"
              onChange={(e) => setgender(e.target.value)}
            >
              <option>Choose Gender</option>
              <option>Male</option>
              <option>Female</option>
            </Form.Control>
            <Form.Control
              as="select"
              className="selectControl mb-3"
              onChange={(e) => setrole(e.target.value)}
            >
              <option>Choose Role</option>
              <option>Admin</option>
              <option>Cashier</option>
              <option>Registrar</option>
            </Form.Control>
            <TextField
              label="address"
              type="text"
              textValue={address}
              onChange={(e) => setaddress(e.target.value)}
              placeholder="Address"
              controlId="addressId"
            />
            <TextField
              label="Mobile Number"
              type="text"
              textValue={mobileNo}
              onChange={(e) => setmobileNo(e.target.value)}
              placeholder="Mobile No"
              controlId="mobileNoId"
            />
            <TextField
              label="Username"
              type="text"
              textValue={username}
              onChange={(e) => setusername(e.target.value)}
              placeholder="Username"
              controlId="usernameId"
            />
            <TextField
              label="Password"
              type="password"
              textValue={password}
              onChange={(e) => setpassword(e.target.value)}
              placeholder="Password"
              controlId="passwordId"
            />
          </Modal.Body>
          <Modal.Footer>
            <Button variant="secondary" onClick={handleClose}>
              Close
            </Button>
            <Button variant="primary" onClick={handleCreate}>
              Create Admin
            </Button>
          </Modal.Footer>
        </Modal>
      </>
    );
};

export default AdminView;
