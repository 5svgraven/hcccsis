import { useState, useEffect, useContext } from "react";
import { Button, Table, Form, Row, Col, Modal } from "react-bootstrap";
import {
  GetAllTeacher,
  GetOneTeacher,
  UpdateTeacher,
  DeleteTeacher,
  CreateTeacher,
} from "../../services/teacher_service";
import {
  AssignTeacherToSubject,
  GetAllCourseSubject,
  GetAllClassSubject,
} from "../../services/subject_service";
import TextField from "../../component/textfield/hccc_textfield";
import swal from "sweetalert";
import UserContext from "../../context/UserContext";
import Loading from "../../component/loading";

const Teacher = () => {
  const { user } = useContext(UserContext);
  const [data, setData] = useState([]);
  const [courseData, setCourseData] = useState([]);
  const [classData, setClassData] = useState([]);
  const [showCreateModal, setShowCreateModal] = useState(false);
  const [showUpdateModal, setShowUpdateModal] = useState(false);
  const [showAssignModal, setShowAssignModal] = useState(false);
  const [isBusy, setBusy] = useState(false);
  //data Teacher
  const [firstname, setfirstname] = useState("");
  const [lastname, setlastname] = useState("");
  const [age, setage] = useState("");
  const [birthday, setbirthday] = useState("");
  const [gender, setgender] = useState("");
  const [address, setaddress] = useState("");
  const [mobileNo, setmobileNo] = useState("");
  const [specialization, setspecialization] = useState("");
  const [teacherId, setteacherId] = useState("");
  const [username, setusername] = useState("");
  const [password, setpassword] = useState("");
  const [searchText, setSearchText] = useState("");

  const handleClose = () => {
    setShowCreateModal(false);
    setShowUpdateModal(false);
    setShowAssignModal(false);
  };

  useEffect(() => {
    let isSubscribed = true;
    getAllTeacher();
    getAllCourse();
    getAllSection();
    return () => {
      isSubscribed = false;
    };
  }, [showCreateModal, showUpdateModal, showAssignModal]);

  //TODO Get all Teacher
  async function getAllTeacher() {
    setData(await GetAllTeacher());
  }

  //TODO Get all Course Subject
  async function getAllCourse() {
    setCourseData(await GetAllCourseSubject());
  }
  //TODO Get all Section Subject
  async function getAllSection() {
    setClassData(await GetAllClassSubject());
  }

  //TODO get one Teacher
  async function getOneTeacher(teachId) {
    const result = await GetOneTeacher(teachId);
    setfirstname(result.firstname);
    setlastname(result.lastname);
    setage(result.age);
    setaddress(result.address);
    setmobileNo(result.mobileNo);
    setspecialization(result.specialization);
  }
  //TODO create Teacher
  async function handleCreate() {
    const result = await CreateTeacher(
      firstname,
      lastname,
      age,
      gender,
      birthday,
      address,
      mobileNo,
      specialization,
      username,
      password
    );
    if (result)
      swal("Poof! Teacher has been created!", {
        icon: "success",
      });
    handleClose();
  }

  //TODO update Teacher
  async function handleUpdate() {
    setBusy(true);
    const result = await UpdateTeacher(
      teacherId,
      firstname,
      lastname,
      age,
      address,
      mobileNo,
      specialization
    );
    setBusy(false);
    if (result)
      swal("Poof! Teacher has been updated!", {
        icon: "success",
      });
    handleClose();
  }

  //TODO delete a Teacher
  async function handleDelete(teacherId) {
    const willDelete = await swal({
      title: "Are you sure?",
      text: "Once deleted, you will not be able to recover this Teacher!",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    });

    if (willDelete) {
      const result = await DeleteTeacher(teacherId);
      if (result) {
        swal("Poof! Teacher has been deleted!", {
          icon: "success",
        });
        handleClose();
        getAllTeacher();
      } else {
        swal("Opss! Teacher deletion failed!", {
          icon: "error",
        });
        handleClose();
      }
    } else {
    }
    getAllTeacher();
    handleClose();
  }

  //TODO Assign a Teacher to a Subject
  async function assignTeacherToSubject(subjectId) {
    setBusy(true);
    const result = await AssignTeacherToSubject(subjectId, teacherId);
    setBusy(false);
    if (result)
      swal("Poof! Teacher has been assigned!", {
        icon: "success",
      });
    else
      swal("Opss! Teacher assigning failed!", {
        icon: "error",
      });
    getAllTeacher();
    handleClose();
  }

  const showCourseSubject = courseData.map((course) => {
    return (
      <tr key={course._id}>
        <td>{course.name}</td>
        <td>{course.teacher}</td>
        <td>{course.subjectStart}</td>
        <td>{course.subjectEnd}</td>
        <td>{course.room}</td>
        <td>
          <Button
            variant="info"
            onClick={() => {
              assignTeacherToSubject(course._id);
              getAllCourse();
            }}
          >
            SELECT
          </Button>
        </td>
      </tr>
    );
  });

  const showClassSubject = classData.map((cls) => {
    return (
      <tr key={cls._id}>
        <td>{cls.name}</td>
        <td>{cls.teacher}</td>
        <td>{cls.subjectStart}</td>
        <td>{cls.subjectEnd}</td>
        <td>{cls.room}</td>
        <td>{cls.maxStudent}</td>
        <td>
          <Button
            variant="info"
            onClick={() => {
              assignTeacherToSubject(cls._id, teacherId);
              getAllSection();
            }}
          >
            SELECT
          </Button>
        </td>
      </tr>
    );
  });

  const filteredTeacher = data.filter((teacher) => {
    if (searchText === "") {
      return teacher;
    } else {
      return (
        teacher.firstname.toLowerCase().indexOf(searchText.toLowerCase()) !== -1
      );
    }
  });

  const showTeachers = filteredTeacher.map((teacher) => {
    return (
      <tr key={teacher._id}>
        <td>{teacher.firstname + " " + teacher.lastname}</td>
        <td>{teacher.age}</td>
        <td>{teacher.gender}</td>
        <td>{teacher.mobileNo}</td>
        <td>{teacher.specialization}</td>
        <td>
          <div className="d-flex justify-content-around">
            <Button
              onClick={() => {
                getOneTeacher(teacher._id);
                setteacherId(teacher._id);
                setShowUpdateModal(true);
              }}
              variant="warning"
            >
              Update
            </Button>
            <Button
              variant="info"
              onClick={() => {
                setShowAssignModal(true);
                getOneTeacher(teacher._id);
                setteacherId(teacher._id);
              }}
            >
              Assign
            </Button>
            <Button variant="danger" onClick={() => handleDelete(teacher._id)}>
              Delete
            </Button>
          </div>
        </td>
      </tr>
    );
  });

  if (isBusy) return <Loading />;
  else
    return (
      <>
        <h1>Teacher</h1>

        <Row className="justify-content-center">
          <Col md={10}>
            <Row className="d-flex justify-content-between">
              <Button className="my-2" onClick={() => setShowCreateModal(true)}>
                Add New Teacher
              </Button>
              <TextField
                type="text"
                textValue={searchText}
                onChange={(e) => setSearchText(e.target.value)}
                placeholder="Search Teacher"
                controlId="SearchId1"
              />
            </Row>

            <Table striped bordered hover>
              <thead>
                <tr>
                  <th>Name</th>
                  <th>Age</th>
                  <th>Gender</th>
                  <th>Mobile No.</th>
                  <th>Specialization</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>{showTeachers}</tbody>
            </Table>
          </Col>
        </Row>
        <Modal
          show={showUpdateModal}
          onHide={handleClose}
          centered
          size="lg"
          aria-labelledby="updateModal"
        >
          <Modal.Header closeButton>
            <Modal.Title>Update Teacher</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <TextField
              label="First Name"
              type="text"
              textValue={firstname}
              onChange={(e) => setfirstname(e.target.value)}
              placeholder="First Name"
              controlId="firstnameId"
            />
            <TextField
              label="Last Name"
              type="text"
              textValue={lastname}
              onChange={(e) => setlastname(e.target.value)}
              placeholder="Last Name"
              controlId="lastnameId"
            />
            <TextField
              label="Age"
              type="text"
              textValue={age}
              onChange={(e) => setage(e.target.value)}
              placeholder="Age"
              controlId="ageId"
            />
            <TextField
              label="Address"
              type="text"
              textValue={address}
              onChange={(e) => setaddress(e.target.value)}
              placeholder="Address"
              controlId="addressId"
            />
            <TextField
              label="Mobile No"
              type="text"
              textValue={mobileNo}
              onChange={(e) => setmobileNo(e.target.value)}
              placeholder="Mobile No"
              controlId="mobileNoid"
            />
          </Modal.Body>
          <Modal.Footer>
            <Button variant="secondary" onClick={handleClose}>
              Close
            </Button>
            <Button variant="primary" onClick={handleUpdate}>
              Update Teacher
            </Button>
          </Modal.Footer>
        </Modal>
        <Modal
          show={showCreateModal}
          onHide={handleClose}
          centered
          size="lg"
          aria-labelledby="createModal"
        >
          <Modal.Header closeButton>
            <Modal.Title>Create Teacher</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <TextField
              label="First name"
              type="text"
              textValue={firstname}
              onChange={(e) => setfirstname(e.target.value)}
              placeholder="First Name"
              controlId="firstnameId"
            />
            <TextField
              label="Last name"
              type="text"
              textValue={lastname}
              onChange={(e) => setlastname(e.target.value)}
              placeholder="Last Name"
              controlId="lastnameId"
            />
            <TextField
              label="Age"
              type="text"
              textValue={age}
              onChange={(e) => setage(e.target.value)}
              placeholder="Age"
              controlId="ageId"
            />

            <TextField
              label="Birthday"
              type="text"
              textValue={birthday}
              onChange={(e) => setbirthday(e.target.value)}
              placeholder="YYYY-MM-DD use this format"
              controlId="birthdayId"
            />
            <Form.Control
              as="select"
              className="selectControl mb-3"
              onChange={(e) => setgender(e.target.value)}
            >
              <option>Choose Gender</option>
              <option>Male</option>
              <option>Female</option>
            </Form.Control>
            <TextField
              label="address"
              type="text"
              textValue={address}
              onChange={(e) => setaddress(e.target.value)}
              placeholder="Address"
              controlId="addressId"
            />
            <TextField
              label="Mobile Number"
              type="text"
              textValue={mobileNo}
              onChange={(e) => setmobileNo(e.target.value)}
              placeholder="Mobile No"
              controlId="mobileNoId"
            />
            <TextField
              label="Specialization"
              type="text"
              textValue={specialization}
              onChange={(e) => setspecialization(e.target.value)}
              placeholder="Specialization"
              controlId="specializationId"
            />
            <TextField
              label="Username"
              type="text"
              textValue={username}
              onChange={(e) => setusername(e.target.value)}
              placeholder="Username"
              controlId="usernameId"
            />
            <TextField
              label="Password"
              type="password"
              textValue={password}
              onChange={(e) => setpassword(e.target.value)}
              placeholder="Password"
              controlId="passwordId"
            />
          </Modal.Body>
          <Modal.Footer>
            <Button variant="secondary" onClick={handleClose}>
              Close
            </Button>
            <Button variant="primary" onClick={handleCreate}>
              Create Teacher
            </Button>
          </Modal.Footer>
        </Modal>
        <Modal
          show={showAssignModal}
          onHide={handleClose}
          centered
          size="lg"
          aria-labelledby="assignModal"
        >
          <Modal.Header closeButton>
            <Modal.Title>Assign Teacher to Subject</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <Form.Label>Course Subjects</Form.Label>
            <Table striped bordered hover>
              <thead>
                <tr>
                  <th>Title</th>
                  <th>Teacher</th>
                  <th>Subject Start</th>
                  <th>Subject End</th>
                  <th>Room</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>{showCourseSubject}</tbody>
            </Table>
            <Form.Label>Class/Section Subjects</Form.Label>
            <Table striped bordered hover>
              <thead>
                <tr>
                  <th>Title</th>
                  <th>Teacher</th>
                  <th>Subject Start</th>
                  <th>Subject End</th>
                  <th>Room</th>
                  <th>Max Students</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>{showClassSubject}</tbody>
            </Table>
          </Modal.Body>
          <Modal.Footer>
            <Button variant="secondary" onClick={handleClose}>
              Close
            </Button>
          </Modal.Footer>
        </Modal>
      </>
    );
};

export default Teacher;
