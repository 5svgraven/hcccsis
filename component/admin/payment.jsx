import { useState, useEffect, useContext } from "react";
import { Button, Table, Form, Row, Col, Modal } from "react-bootstrap";
import {
  GetAllPayment,
  GetOnePayment,
  UpdatePayment,
  DeletePayment,
  CreatePayment,
} from "../../services/payment_service";
import { GetOneCourse } from "../../services/course_service";
import { GetOneSection } from "../../services/section_service";
import { GetOneStudent } from "../../services/student_service";
import TextField from "../../component/textfield/hccc_textfield";
import swal from "sweetalert";
import UserContext from "../../context/UserContext";
import Loading from "../../component/loading";

const Payment = () => {
  const { user } = useContext(UserContext);
  const [data, setData] = useState([]);
  const [showCreateModal, setShowCreateModal] = useState(false);
  const [showUpdateModal, setShowUpdateModal] = useState(false);
  const [isBusy, setBusy] = useState(false);
  const [showDeleteModal, setShowDeleteModal] = useState(0);
  const [searchText, setsearchText] = useState("");
  //data Payment
  const [name, setname] = useState("");
  const [maxStudent, setmaxStudent] = useState("");
  const [paymentId, setpaymentId] = useState("");
  const [studentId, setstudentId] = useState("");
  const [courseId, setcourseId] = useState("");
  const [sectionId, setsectionId] = useState("");
  const [nameOfCourseOrSection, setNameOfCourseOrSection] = useState("");
  const [remarks, setremarks] = useState("");

  const handleClose = () => {
    setShowCreateModal(false);
    setShowUpdateModal(false);
    setShowDeleteModal(false);
    setname("");
    setpaymentId("");
    setmaxStudent(0);
  };

  useEffect(() => {
    let isSubscribed = true;
    getAllPayment();
    return () => {
      isSubscribed = false;
    };
  }, [showCreateModal, showUpdateModal, showDeleteModal]);

  //TODO Get all Payment
  async function getAllPayment() {
    setData(await GetAllPayment());
  }

  //TODO get one Payment
  async function getOnePayment(paymentId) {
    setBusy(true);
    const result = await GetOnePayment(paymentId);
    setpaymentId(paymentId);
    setstudentId(result.studentId);
    setname(result.name);
    const resultStudent = await GetOneStudent(result.studentId);
    setBusy(false);
    if (resultStudent.courseId) {
      const resultCourse = await GetOneCourse(resultStudent.courseId);
      setNameOfCourseOrSection(await resultCourse.name);
    } else {
      const resultSection = await GetOneSection(resultStudent.sectionId);
      setNameOfCourseOrSection(await resultSection.name);
    }
    return (await result) ? true : false;
  }

  //TODO create Payment
  async function handleCreate() {
    setBusy(true);
    const result = await CreatePayment(name, maxStudent, user.id);
    setBusy(false);
    if (result)
      swal("Poof! Payment has been created!", {
        icon: "success",
      });
    handleClose();
  }

  //handle

  //TODO update Payment
  async function handleUpdate() {
    setBusy(true);
    const result = await UpdatePayment(studentId, remarks, paymentId);
    setBusy(false);
    if (result) {
      swal("Poof! Payment has been updated!", {
        icon: "success",
      });
    } else {
      swal("Opss! Something went wrong", {
        icon: "error",
      });
    }
    handleClose();
    getAllPayment();
  }

  //TODO update Payment
  async function handleUpdatePreEnrollment(studId, payId) {
    setBusy(true);
    const result = await UpdatePayment(studId, "Pre-Enrollment", payId);
    setBusy(false);
    if (result) {
      swal("Poof! Payment has been updated!", {
        icon: "success",
      });
    } else {
      swal("Opss! Something went wrong", {
        icon: "error",
      });
    }
    handleClose();
    getAllPayment();
  }

  //TODO delete a Payment
  async function handleDelete(PaymentId) {
    const willDelete = await swal({
      title: "Are you sure?",
      text: "Once deleted, you will not be able to recover this Payment!",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    });
    if (willDelete) {
      setBusy(true);
      const result = await DeletePayment(PaymentId);
      setBusy(false);
      if (result) {
        swal("Poof! Payment has been deleted!", {
          icon: "success",
        });
        getAllPayment();
        handleClose();
      } else {
        swal("Opss! Payment deletion failed!", {
          icon: "error",
        });
        handleClose();
      }
    } else {
      handleClose();
    }
    handleClose();
  }

  //TODO Assign a Payment to a Payment or Payment
  const filteredPayments = data.filter((payment) => {
    if (searchText === "") {
      return payment;
    } else {
      return (
        payment.name.toLowerCase().indexOf(searchText.toLowerCase()) !== -1
      );
    }
  });
  const showPayments = filteredPayments.map((payment) => {
    return (
      <tr key={payment._id}>
        <td>{payment.name}</td>
        <td>{payment.amount}</td>
        <td>{payment.remarks}</td>
        <td>{payment.status ? "Paid" : "Unpaid"}</td>
        <td>
          <div className="d-flex justify-content-around">
            <Button
              onClick={async () => {
                if (payment.remarks === "Pre-Enrollment") {
                  await handleUpdatePreEnrollment(
                    payment.studentId,
                    payment._id
                  );
                } else {
                  getOnePayment(payment._id);
                  if (isBusy != true) setShowUpdateModal(true);
                }
              }}
              variant="warning"
            >
              Update
            </Button>
            <Button variant="danger" onClick={() => handleDelete(payment._id)}>
              Delete
            </Button>
          </div>
        </td>
      </tr>
    );
  });

  if (isBusy) return <Loading />;
  else
    return (
      <>
        <h1>Payment</h1>
        <Row className="justify-content-center">
          <Col md={10}>
            <Row className="d-flex justify-content-between">
              {/* <Button className="my-2" onClick={() => setShowCreateModal(true)}>
                Add New Payment
              </Button> */}
              <TextField
                type="text"
                textValue={searchText}
                onChange={(e) => setsearchText(e.target.value)}
                placeholder="Search Payments"
                controlId="SearchId5"
              />
            </Row>
            <Table striped bordered hover>
              <thead>
                <tr>
                  <th>Name</th>
                  <th>Amount</th>
                  <th>Remarks</th>
                  <th>Status</th>
                  <th>Actions</th>
                </tr>
              </thead>
              <tbody>{showPayments}</tbody>
            </Table>
          </Col>
        </Row>
        <Modal
          show={showUpdateModal}
          onHide={handleClose}
          centered
          size="lg"
          aria-labelledby="updateModal"
        >
          <Modal.Header closeButton>
            <Modal.Title>Update Payment</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <p>
              <strong>Student Name: </strong> {name}
            </p>
            <p>
              <strong>{courseId !== "" ? "Course" : "Class"} Name: </strong>
              {nameOfCourseOrSection}
            </p>
            <Form.Control
              as="textarea"
              row={3}
              onChange={(e) => setremarks(e.target.value)}
              placeholder="Remarks"
            />
          </Modal.Body>
          <Modal.Footer>
            <Button variant="secondary" onClick={handleClose}>
              Close
            </Button>
            <Button variant="primary" onClick={() => handleUpdate()}>
              Update Payment
            </Button>
          </Modal.Footer>
        </Modal>
        <Modal
          show={showCreateModal}
          onHide={handleClose}
          centered
          size="lg"
          aria-labelledby="createModal"
        >
          <Modal.Header closeButton>
            <Modal.Title>Create Payment</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <TextField
              label="Name"
              type="text"
              textValue={name}
              onChange={(e) => setname(e.target.value)}
              placeholder="Name"
              controlId="nameId"
            />
            <TextField
              label="Max Student"
              type="text"
              textValue={maxStudent}
              onChange={(e) => maxStudent(e.target.value)}
              placeholder="Max Student"
              controlId="maxStudentId"
            />
          </Modal.Body>
          <Modal.Footer>
            <Button variant="secondary" onClick={handleClose}>
              Close
            </Button>
            <Button variant="primary" onClick={handleCreate}>
              Create Payment
            </Button>
          </Modal.Footer>
        </Modal>
      </>
    );
};

export default Payment;
