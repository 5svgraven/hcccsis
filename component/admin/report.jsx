import { useState, useEffect, useRef } from "react";
import { Tab, Row, Col, Nav, Button, Modal, Table } from "react-bootstrap";
import StudentReport from "../report/student_report";
import AcademicReport from "../report/academic_report";
import EnrollmentSummary from "../report/enrollment_summary";
import { useReactToPrint } from "react-to-print";
import { GetAllStudent } from "../../services/student_service";
import { GetAllSubjectStudent } from "../../services/grades_service";
import { GetOneStudent } from "../../services/student_service";
import TextField from "../../component/textfield/hccc_textfield";

const Report = () => {
  const render = "rerender";
  const studentReportRef = useRef();
  const academicReportRef = useRef();
  const enrollmentSummary = useRef();
  const [data, setdata] = useState([]);
  const [arData, setarData] = useState([]);
  const [showStudents, setshowStudents] = useState(false);
  const [studentId, setstudentId] = useState("");
  const [searchStudent, setsearchStudent] = useState("");
  const [studentInfo, setstudentInfo] = useState({
    firstname: null,
    lastname: null,
    age: null,
    gender: null,
    mobileNo: null,
    email: null,
    role: null,
    status: null,
  });

  const handleClose = () => {
    setshowStudents(false);
  };

  const handlePrintStudent = useReactToPrint({
    content: () => studentReportRef.current,
  });

  const handlePrintAcademic = useReactToPrint({
    content: () => academicReportRef.current,
  });

  const handlePrintEnrollmentSummary = useReactToPrint({
    content: () => enrollmentSummary.current,
  });

  useEffect(() => {
    getAllStudent();
  }, [render]);

  async function getAllSubjectStudent(studentId) {
    const result = await GetAllSubjectStudent(studentId);
    const resultOne = await GetOneStudent(studentId);
    setarData(result);
    setstudentInfo({
      firstname: resultOne.firstname,
      lastname: resultOne.lastname,
      age: resultOne.age,
      gender: resultOne.gender,
      mobileNo: resultOne.mobileNo,
      email: resultOne.email,
      role: result.rol,
      status: resultOne.status,
    });
  }

  let totalStudent = 0;
  let classStudent = 0;
  let courseStudent = 0;
  async function getAllStudent() {
    const result = await GetAllStudent();
    setdata(result);
  }
  data.map((student) => {
    if (student.status === "Enrolled") {
      totalStudent += 1;
      if (student.courseId !== "") {
        courseStudent += 1;
      } else {
        classStudent += 1;
      }
    }
  });

  const listOfStudent = data.map((student) => {
    return (
      <tr key={student._id}>
        <td>{student.firstname + " " + student.lastname}</td>
        <td>
          <Button
            variant="success"
            onClick={() => {
              setstudentId(student._id);
              swal("Poof! Student has been set!", {
                icon: "success",
              });
              getAllSubjectStudent(student._id);
              handleClose();
            }}
          >
            SELECT STUDENT
          </Button>
        </td>
      </tr>
    );
  });

  return (
    <>
      <Tab.Container id="left-tabs-example" defaultActiveKey="studentReport">
        <Row>
          <Col sm={2}>
            <Nav variant="pills" className="flex-column">
              <Nav.Item>
                <Nav.Link eventKey="studentReport">Student Report</Nav.Link>
                <Nav.Link eventKey="academicReport">Academic Report</Nav.Link>
                <Nav.Link eventKey="enrollmentSummary">
                  Enrollment Summary
                </Nav.Link>
              </Nav.Item>
            </Nav>
          </Col>
          <Col sm={10}>
            <Tab.Content>
              <Tab.Pane eventKey="studentReport">
                <Button variant="success" onClick={() => setshowStudents(true)}>
                  Pick Student
                </Button>
                <Button variant="warning" onClick={handlePrintStudent}>
                  Print Student Report
                </Button>
                <StudentReport
                  ref={studentReportRef}
                  studentInfo={studentInfo}
                />
              </Tab.Pane>
              <Tab.Pane eventKey="academicReport">
                <Button variant="success" onClick={() => setshowStudents(true)}>
                  Pick Student
                </Button>
                <Button
                  variant="warning"
                  disabled={studentId === "" ? true : false}
                  onClick={handlePrintAcademic}
                >
                  Print Academic Report
                </Button>
                <AcademicReport ref={academicReportRef} arData={arData} />
              </Tab.Pane>
              <Tab.Pane eventKey="enrollmentSummary">
                <Button variant="info" onClick={handlePrintEnrollmentSummary}>
                  Enrollment Summary
                </Button>
                <EnrollmentSummary
                  ref={enrollmentSummary}
                  totalStudent={totalStudent}
                  courseStudent={courseStudent}
                  classStudent={classStudent}
                />
              </Tab.Pane>
            </Tab.Content>
          </Col>
        </Row>
      </Tab.Container>
      <Modal
        show={showStudents}
        onHide={handleClose}
        centered
        size="lg"
        aria-labelledby="showstudentId"
      >
        <Modal.Header closeButton>
          <Modal.Title>Students</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <TextField
            type="text"
            textValue={searchStudent}
            onChange={(e) => setsearchStudent(e.target.value)}
            placeholder="Search Student"
            controlId="SearchId8"
          />
          <Table striped bordered hover>
            <thead>
              <tr>
                <td>Student Name</td>
                <td>Action</td>
              </tr>
            </thead>
            <tbody>{listOfStudent}</tbody>
          </Table>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="info" onClick={handleClose}>
            Back
          </Button>
        </Modal.Footer>
      </Modal>
    </>
  );
};

export default Report;
