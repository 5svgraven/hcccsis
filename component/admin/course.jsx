import { useState, useEffect, useContext } from "react";
import { Button, Table, Form, Row, Col, Modal } from "react-bootstrap";
import {
  GetAllCourse,
  GetOneCourse,
  UpdateCourse,
  DeleteCourse,
  CreateCourse,
} from "../../services/course_service";
import TextField from "../../component/textfield/hccc_textfield";
import swal from "sweetalert";
import UserContext from "../../context/UserContext";
import Loading from "../../component/loading";

const Course = () => {
  const { user } = useContext(UserContext);
  const [data, setData] = useState([]);
  const [showCreateModal, setShowCreateModal] = useState(false);
  const [showUpdateModal, setShowUpdateModal] = useState(false);
  const [showDeleteModal, setShowDeleteModal] = useState(0);
  const [isBusy, setBusy] = useState(false);
  const [searchText, setsearchText] = useState("");
  //data Course
  const [name, setname] = useState("");
  const [courseId, setcourseId] = useState("");

  const handleClose = () => {
    setShowCreateModal(false);
    setShowUpdateModal(false);
    setShowDeleteModal(false);
    setname("");
  };

  useEffect(() => {
    let isSubscribed = true;
    getAllCourse();
    return () => {
      isSubscribed = false;
    };
  }, [showCreateModal, showUpdateModal, showDeleteModal]);

  //TODO Get all Course
  async function getAllCourse() {
    setData(await GetAllCourse());
  }

  //TODO get one Course
  async function getOneCourse(courseId) {
    setBusy(true);
    const result = await GetOneCourse(courseId);
    setBusy(false);
    setname(result.name);
  }

  //TODO create Course
  async function handleCreate() {
    setBusy(true);
    const result = await CreateCourse(name, user.id);
    setBusy(false);
    if (result)
      swal("Poof! Course has been created!", {
        icon: "success",
      });
    getAllCourse();
    handleClose();
  }

  //TODO update Course
  async function handleUpdate() {
    const result = await UpdateCourse(courseId, name);
    if (result)
      swal("Poof! Course has been updated!", {
        icon: "success",
      });
    handleClose();
    getAllCourse();
  }

  //TODO delete a Course
  async function handleDelete(courseId) {
    const willDelete = await swal({
      title: "Are you sure?",
      text: "Once deleted, you will not be able to recover this Course!",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    });
    if (willDelete) {
      setBusy(true);
      const result = await DeleteCourse(courseId);
      setBusy(false);
      if (result) {
        swal("Poof! Course has been deleted!", {
          icon: "success",
        });
        getAllCourse();
        handleClose();
      } else {
        swal("Opss! Course deletion failed!", {
          icon: "error",
        });
        handleClose();
      }
    } else {
      handleClose();
    }
    getAllCourse();
    handleClose();
  }

  //TODO Assign a Course to a Course or Course

  const filteredCourse = data.filter((course) => {
    if (searchText === "") {
      return course;
    } else {
      return course.name.toLowerCase().indexOf(searchText.toLowerCase()) !== -1;
    }
  });
  const showCourses = filteredCourse.map((course) => {
    return (
      <tr key={course._id}>
        <td>{course.name}</td>
        <td>
          <div className="d-flex justify-content-around">
            <Button
              onClick={() => {
                setShowUpdateModal(true);
                getOneCourse(course._id);
                setcourseId(course._id);
              }}
              variant="warning"
            >
              Update
            </Button>
            <Button variant="danger" onClick={() => handleDelete(course._id)}>
              Delete
            </Button>
          </div>
        </td>
      </tr>
    );
  });

  if (isBusy) return <Loading />;
  else
    return (
      <>
        <h1>Course</h1>
        <Row className="justify-content-center">
          <Col md={10}>
            <Row className="d-flex justify-content-between">
              <Button className="my-2" onClick={() => setShowCreateModal(true)}>
                Add New Course
              </Button>
              <TextField
                type="text"
                textValue={searchText}
                onChange={(e) => setsearchText(e.target.value)}
                placeholder="Search Course"
                controlId="SearchId6"
              />
            </Row>
            <Table striped bordered hover>
              <thead>
                <tr>
                  <th>Name</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>{showCourses}</tbody>
            </Table>
          </Col>
        </Row>
        <Modal
          show={showUpdateModal}
          onHide={handleClose}
          centered
          size="lg"
          aria-labelledby="updateModal"
        >
          <Modal.Header closeButton>
            <Modal.Title>Update Course</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <TextField
              label="Name"
              type="text"
              textValue={name}
              onChange={(e) => setname(e.target.value)}
              placeholder="Name"
              controlId="nameId"
            />
          </Modal.Body>
          <Modal.Footer>
            <Button variant="secondary" onClick={handleClose}>
              Close
            </Button>
            <Button variant="primary" onClick={handleUpdate}>
              Update Course
            </Button>
          </Modal.Footer>
        </Modal>
        <Modal
          show={showCreateModal}
          onHide={handleClose}
          centered
          size="lg"
          aria-labelledby="createModal"
        >
          <Modal.Header closeButton>
            <Modal.Title>Create Course</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <TextField
              label="Name"
              type="text"
              textValue={name}
              onChange={(e) => setname(e.target.value)}
              placeholder="Name"
              controlId="nameId"
            />
          </Modal.Body>
          <Modal.Footer>
            <Button variant="secondary" onClick={handleClose}>
              Close
            </Button>
            <Button variant="primary" onClick={handleCreate}>
              Create Course
            </Button>
          </Modal.Footer>
        </Modal>
      </>
    );
};

export default Course;
