import { useState, useEffect, useContext } from "react";
import { Button, Table, Form, Row, Col, Modal } from "react-bootstrap";
import {
  GetAllSection,
  GetOneSection,
  UpdateSection,
  DeleteSection,
  CreateSection,
} from "../../services/section_service";
import TextField from "../../component/textfield/hccc_textfield";
import swal from "sweetalert";
import UserContext from "../../context/UserContext";
import Loading from "../../component/loading";

const Section = () => {
  const { user } = useContext(UserContext);
  const [data, setData] = useState([]);
  const [subjects, setSubjects] = useState([]);
  const [showCreateModal, setShowCreateModal] = useState(false);
  const [showUpdateModal, setShowUpdateModal] = useState(false);
  const [isBusy, setBusy] = useState(false);
  const [searchText, setsearchText] = useState("");

  //data Section
  const [name, setname] = useState("");
  const [maxStudent, setmaxStudent] = useState("");
  const [sectionId, setsectionId] = useState("");
  const [subjectId, setSubjectId] = useState("");

  const handleClose = () => {
    setShowCreateModal(false);
    setShowUpdateModal(false);
    setname("");
    setmaxStudent(0);
  };

  useEffect(() => {
    let isSubscribed = true;
    getAllSection();
    return () => {
      isSubscribed = false;
    };
  }, [showCreateModal, showUpdateModal]);

  //TODO Get all Section
  async function getAllSection() {
    setData(await GetAllSection());
  }

  //TODO get one Section
  async function getOneSection(sectionId) {
    const result = await GetOneSection(sectionId);
    setname(result.name);
    setmaxStudent(result.maxStudent);
  }

  //TODO create Section
  async function handleCreate() {
    setBusy(true);
    const result = await CreateSection(name, maxStudent, user.id);
    setBusy(false);
    if (result)
      swal("Poof! Section has been created!", {
        icon: "success",
      });
    handleClose();
  }

  //TODO update Section
  async function handleUpdate() {
    setBusy(true);
    const result = await UpdateSection(sectionId, name, maxStudent);
    setBusy(false);
    if (result)
      swal("Poof! Section has been updated!", {
        icon: "success",
      });

    handleClose();
    getAllSection();
  }

  //TODO delete a Section
  async function handleDelete(sectionId) {
    const willDelete = await swal({
      title: "Are you sure?",
      text: "Once deleted, you will not be able to recover this Section!",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    });
    if (willDelete) {
      setBusy(true);
      const result = await DeleteSection(sectionId);
      setBusy(false);
      if (result) {
        swal("Poof! Section has been deleted!", {
          icon: "success",
        });
        getAllSection();
        handleClose();
      } else {
        swal("Opss! Section deletion failed!", {
          icon: "error",
        });
        handleClose();
      }
    } else {
      handleClose();
    }
    handleClose();
  }

  //TODO Assign a Section to a Section or Section

  const filteredSection = data.filter((section) => {
    if (searchText === "") {
      return section;
    } else {
      return (
        section.name.toLowerCase().indexOf(searchText.toLowerCase()) !== -1
      );
    }
  });
  const showSections = filteredSection.map((section) => {
    return (
      <tr key={section._id}>
        <td>{section.name}</td>
        <td>{section.maxStudent}</td>
        <td>
          <div className="d-flex justify-content-around">
            <Button
              onClick={() => {
                setShowUpdateModal(true);
                getOneSection(section._id);
                setsectionId(section._id);
              }}
              variant="warning"
            >
              Update
            </Button>
            <Button variant="danger" onClick={() => handleDelete(section._id)}>
              Delete
            </Button>
          </div>
        </td>
      </tr>
    );
  });

  if (isBusy) return <Loading />;
  else
    return (
      <>
        <h1>Section</h1>
        <Row className="justify-content-center">
          <Col md={10}>
            <Row className="d-flex justify-content-between">
              <Button className="my-2" onClick={() => setShowCreateModal(true)}>
                Add New Section
              </Button>
              <TextField
                type="text"
                textValue={searchText}
                onChange={(e) => setsearchText(e.target.value)}
                placeholder="Search Section"
                controlId="SearchId4"
              />
            </Row>
            <Table striped bordered hover>
              <thead>
                <tr>
                  <th>Name</th>
                  <th>Max Student</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>{showSections}</tbody>
            </Table>
          </Col>
        </Row>
        <Modal
          show={showUpdateModal}
          onHide={handleClose}
          centered
          size="lg"
          aria-labelledby="updateModal"
        >
          <Modal.Header closeButton>
            <Modal.Title>Update Section</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <TextField
              label="Name"
              type="text"
              textValue={name}
              onChange={(e) => setname(e.target.value)}
              placeholder="Name"
              controlId="nameId"
            />
            <TextField
              label="Max Student"
              type="text"
              textValue={maxStudent}
              onChange={(e) => setmaxStudent(e.target.value)}
              placeholder="Max Student"
              controlId="maxStudentId"
            />
          </Modal.Body>
          <Modal.Footer>
            <Button variant="secondary" onClick={handleClose}>
              Close
            </Button>
            <Button variant="primary" onClick={handleUpdate}>
              Update Section
            </Button>
          </Modal.Footer>
        </Modal>
        <Modal
          show={showCreateModal}
          onHide={handleClose}
          centered
          size="lg"
          aria-labelledby="createModal"
        >
          <Modal.Header closeButton>
            <Modal.Title>Create Section</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <TextField
              label="Name"
              type="text"
              textValue={name}
              onChange={(e) => setname(e.target.value)}
              placeholder="Name"
              controlId="nameId"
            />
            <TextField
              label="Max Student"
              type="text"
              textValue={maxStudent}
              onChange={(e) => setmaxStudent(e.target.value)}
              placeholder="Max Student"
              controlId="maxStudentId"
            />
          </Modal.Body>
          <Modal.Footer>
            <Button variant="secondary" onClick={handleClose}>
              Close
            </Button>
            <Button variant="primary" onClick={handleCreate}>
              Create Section
            </Button>
          </Modal.Footer>
        </Modal>
      </>
    );
};

export default Section;
