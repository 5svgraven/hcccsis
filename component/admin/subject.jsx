import { useState, useEffect, useContext } from "react";
import { Button, Table, Form, Row, Col, Modal } from "react-bootstrap";
import {
  GetAllSubject,
  GetOneSubject,
  UpdateSubject,
  DeleteSubject,
  CreateSubject,
} from "../../services/subject_service";
import {
  GetAllSection,
  AssignSubjectSection,
} from "../../services/section_service";
import {
  GetAllCourse,
  AssignSubjectCourse,
} from "../../services/course_service";
import TextField from "../../component/textfield/hccc_textfield";
import swal from "sweetalert";
import UserContext from "../../context/UserContext";
import Loading from "../../component/loading";

const Subject = () => {
  const { user } = useContext(UserContext);
  const [data, setData] = useState([]);
  const [sectionData, setSectionData] = useState([]);
  const [courseData, setCourseData] = useState([]);
  const [showCreateModal, setShowCreateModal] = useState(false);
  const [showUpdateModal, setShowUpdateModal] = useState(false);
  const [showDeleteModal, setShowDeleteModal] = useState(false);
  const [showAssignModal, setShowAssignModal] = useState(false);
  const [isClass, setIsClass] = useState(false);
  const [isBusy, setBusy] = useState(false);
  const [searchText, setsearchText] = useState("");
  //data Subject
  const [name, setname] = useState("");
  const [subjectType, setsubjectType] = useState("");
  const [teacher, setteacher] = useState("");
  const [subjectStart, setsubjectStart] = useState("");
  const [subjectEnd, setsubjectEnd] = useState("");
  const [room, setroom] = useState("");
  const [maxStudent, setmaxStudent] = useState("");
  const [selectedSubject, setSelectedSubject] = useState("");
  const [subjectId, setsubjectId] = useState("");

  const handleClose = () => {
    setShowCreateModal(false);
    setShowUpdateModal(false);
    setShowDeleteModal(false);
    setShowAssignModal(false);
    setname("");
    setsubjectType("");
    setSelectedSubject("");
  };

  useEffect(() => {
    let isSubscribed = true;
    getAllSubject();
    getAllSection();
    getAllCourse();
    return () => {
      isSubscribed = false;
    };
  }, [showCreateModal, showUpdateModal, showDeleteModal]);
  //TODO Get all Subject
  async function getAllSubject() {
    setBusy(true);
    const result = await GetAllSubject();
    setBusy(false);
    setData(result);
  }

  //TODO Get All Section
  async function getAllSection() {
    setSectionData(await GetAllSection());
  }
  //TODO Get all Course
  async function getAllCourse() {
    setCourseData(await GetAllCourse());
  }

  //TODO get one Subject
  async function getOneSubject(subjectId) {
    const result = await GetOneSubject(subjectId);
    result.subjectType;
    setname(result.name);
    setsubjectType(result.subjectType);
    setteacher(result.teacher);
    setsubjectStart(result.subjectStart);
    setsubjectEnd(result.subjectEnd);
    setroom(result.room);
    setmaxStudent(result.maxStudent);
  }
  //TODO create Subject
  async function handleCreate() {
    setBusy(true);
    const result = await CreateSubject(name, subjectType, user.id);
    setBusy(false);
    if (result)
      swal("Poof! Subject has been created!", {
        icon: "success",
      });
    handleClose();
  }

  //TODO update Subject
  async function handleUpdate() {
    setBusy(true);
    const result = await UpdateSubject(
      subjectId,
      name,
      subjectType,
      teacher,
      subjectStart,
      subjectEnd,
      room,
      maxStudent
    );
    setBusy(false);
    if (result)
      swal("Poof! Subject has been updated!", {
        icon: "success",
      });
    handleClose();
  }

  //TODO delete a Subject
  async function handleDelete(subjectId) {
    const willDelete = await swal({
      title: "Are you sure?",
      text: "Once deleted, you will not be able to recover this Subject!",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    });
    if (willDelete) {
      setBusy(true);
      const result = await DeleteSubject(subjectId);
      setBusy(false);
      if (result) {
        swal("Poof! Subject has been deleted!", {
          icon: "success",
        });
        getAllSubject();
        handleClose();
      } else {
        swal("Opss! Subject deletion failed!", {
          icon: "error",
        });
        handleClose();
      }
    } else {
      handleClose();
    }
    handleClose();
  }

  async function handleSelect(studyType, referenceId) {
    setBusy(true);
    if (studyType === "Section") {
      const result = await AssignSubjectSection(referenceId, selectedSubject);
      if (result) {
        setBusy(false);
        swal("Successfully Added", {
          icon: "success",
        });
      } else {
        setBusy(false);
        swal("Failed. Try Again", {
          icon: "error",
        });
      }
      getAllSection();
    } else {
      setBusy(true);
      const result = await AssignSubjectCourse(referenceId, selectedSubject);
      if (result) {
        setBusy(false);
        swal("Successfully Added", {
          icon: "success",
        });
      } else {
        setBusy(false);
        swal("Failed. Try Again", {
          icon: "error",
        });
      }
      getAllCourse();
    }
  }

  //TODO Assign a Subject to a Section or Subject

  const showSections = sectionData.map((section) => {
    return (
      <tr key={section._id}>
        <td>{section.name}</td>
        <td>{section.maxStudent}</td>
        <td>{section.subjects.length}</td>
        <td>{section.students.length}</td>
        <td className="text-center">
          <Button
            variant="info"
            onClick={() => {
              handleSelect("Section", section._id);
            }}
          >
            SELECT
          </Button>
        </td>
      </tr>
    );
  });

  const showCourses = courseData.map((course) => {
    return (
      <tr key={course._id}>
        <td>{course.name}</td>
        <td>{course.subjects.length}</td>
        <td>{course.students.length}</td>
        <td className="text-center">
          <Button
            variant="info"
            onClick={() => {
              handleSelect("Course", course._id);
            }}
          >
            SELECT
          </Button>
        </td>
      </tr>
    );
  });

  const filteredSubjects = data.filter((subject) => {
    if (searchText === "") {
      return subject;
    } else {
      return (
        subject.name.toLowerCase().indexOf(searchText.toLowerCase()) !== -1
      );
    }
  });
  const showSubjects = filteredSubjects.map((subject) => {
    return (
      <tr key={subject._id}>
        <td>{subject.name}</td>
        <td>{subject.subjectType}</td>
        <td>{subject.teacher}</td>
        <td>{subject.subjectStart}</td>
        <td>{subject.subjectEnd}</td>
        <td>{subject.room}</td>
        <td>{subject.maxStudent}</td>
        <td>
          <div className="d-flex justify-content-around">
            <Button
              onClick={() => {
                setShowUpdateModal(true);
                getOneSubject(subject._id);
                setsubjectId(subject._id);
              }}
              variant="warning"
            >
              Update
            </Button>
            <Button
              variant="info"
              onClick={() => {
                if (subject.subjectType === "Class") setIsClass(true);
                else setIsClass(false);
                setSelectedSubject(subject._id);
                setShowAssignModal(true);
              }}
            >
              Assign
            </Button>
            <Button variant="danger" onClick={() => handleDelete(subject._id)}>
              Delete
            </Button>
          </div>
        </td>
      </tr>
    );
  });

  if (isBusy) return <Loading />;
  else
    return (
      <>
        <h1>Subject</h1>
        <Row className="justify-content-center">
          <Col md={10}>
            <Row className="d-flex justify-content-between">
              <Button className="my-2" onClick={() => setShowCreateModal(true)}>
                Add New Subject
              </Button>
              <TextField
                type="text"
                textValue={searchText}
                onChange={(e) => setsearchText(e.target.value)}
                placeholder="Search Subject"
                controlId="SearchId2"
              />
            </Row>

            <Table striped bordered hover>
              <thead>
                <tr>
                  <th>Title</th>
                  <th>Subject Type</th>
                  <th>Teacher</th>
                  <th>Subject Start</th>
                  <th>Subject End</th>
                  <th>Room</th>
                  <th>Max Students</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>{showSubjects}</tbody>
            </Table>
          </Col>
        </Row>
        <Modal
          show={showUpdateModal}
          onHide={handleClose}
          centered
          size="lg"
          aria-labelledby="updateModal"
        >
          <Modal.Header closeButton>
            <Modal.Title>Update Subject</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <TextField
              label="Title"
              type="text"
              textValue={name}
              onChange={(e) => setname(e.target.value)}
              placeholder="name"
              controlId="subjectId"
            />
            <p>Subject Type</p>
            <Form.Control
              as="select"
              className="selectControl mb-3"
              value={subjectType}
              onChange={(e) => setsubjectType(e.target.value)}
            >
              <option>Choose Subject Type</option>
              <option>Class</option>
              <option>Course</option>
            </Form.Control>
            <TextField
              label="Teacher"
              type="text"
              textValue={teacher}
              onChange={(e) => setteacher(e.target.value)}
              placeholder="Teacher"
              controlId="teacherId"
            />
            <TextField
              label="Subject Start at"
              type="text"
              textValue={subjectStart}
              onChange={(e) => setsubjectStart(e.target.value)}
              placeholder="Time Start"
              controlId="subjectStartId"
            />
            <TextField
              label="Subject End at"
              type="text"
              textValue={subjectEnd}
              onChange={(e) => setsubjectEnd(e.target.value)}
              placeholder="Time End"
              controlId="subjectEndId"
            />
            <TextField
              label="Room"
              type="text"
              textValue={room}
              onChange={(e) => setroom(e.target.value)}
              placeholder="Room"
              controlId="roomId"
            />
            {subjectType != "Class" ? (
              <div></div>
            ) : (
              <TextField
                label="Max Students"
                type="text"
                textValue={maxStudent}
                onChange={(e) => setmaxStudent(e.target.value)}
                placeholder="Max Students"
                controlId="maxStudentId"
              />
            )}
          </Modal.Body>
          <Modal.Footer>
            <Button variant="secondary" onClick={handleClose}>
              Close
            </Button>
            <Button variant="primary" onClick={handleUpdate}>
              Update Subject
            </Button>
          </Modal.Footer>
        </Modal>
        <Modal
          show={showCreateModal}
          onHide={handleClose}
          centered
          size="lg"
          aria-labelledby="createModal"
        >
          <Modal.Header closeButton>
            <Modal.Title>Create Subject</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <TextField
              label="Title"
              type="text"
              textValue={name}
              onChange={(e) => setname(e.target.value)}
              placeholder="Subject Title"
              controlId="subjectId"
            />
            <p>Select Subject Type</p>
            <Form.Control
              as="select"
              className="mb-3"
              value={subjectType}
              onChange={(e) => setsubjectType(e.target.value)}
            >
              <option>Choose Subject Type</option>
              <option>Class</option>
              <option>Course</option>
            </Form.Control>
          </Modal.Body>
          <Modal.Footer>
            <Button variant="secondary" onClick={handleClose}>
              Close
            </Button>
            <Button variant="primary" onClick={handleCreate}>
              Create Subject
            </Button>
          </Modal.Footer>
        </Modal>
        <Modal
          show={showAssignModal}
          onHide={handleClose}
          centered
          size="lg"
          aria-labelledby="assignModal"
        >
          <Modal.Header closeButton>
            <Modal.Title>
              Assign Subject to a {isClass ? "Class" : "Course"}
            </Modal.Title>
          </Modal.Header>
          <Modal.Body>
            {isClass ? (
              <>
                <p>Section/Class</p>
                <Table striped bordered hover>
                  <thead>
                    <tr>
                      <th>Section/Class</th>
                      <th>Max Student</th>
                      <th>No. of Subjects</th>
                      <th>No. of Students</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tbody>{showSections}</tbody>
                </Table>
              </>
            ) : (
              <>
                <p>Course</p>
                <Table striped bordered hover>
                  <thead>
                    <tr>
                      <th>Section/Class</th>
                      <th>No. of Subjects</th>
                      <th>No. of Students</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tbody>{showCourses}</tbody>
                </Table>
              </>
            )}
          </Modal.Body>
          <Modal.Footer>
            <Button variant="secondary" onClick={handleClose}>
              Close
            </Button>
          </Modal.Footer>
        </Modal>
      </>
    );
};

export default Subject;
