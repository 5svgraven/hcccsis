import { useState, useEffect, useContext } from "react";
import { Button, Table, Form, Row, Col, Modal } from "react-bootstrap";
import {
  GetAllStudent,
  GetOneStudent,
  UpdateStudent,
  DeleteStudent,
  CreateStudent,
} from "../../services/student_service";
import { GetOneSection } from "../../services/section_service";
import { GetOneCourse } from "../../services/course_service";
import { CreatePayment } from "../../services/payment_service";
import TextField from "../../component/textfield/hccc_textfield";
import swal from "sweetalert";
import { SendEmailToApplicant } from "../../services/admin_service";
import UserContext from "../../context/UserContext";
import Loading from "../../component/loading";

const Student = () => {
  const renders = "re-renders";
  const [data, setData] = useState([]);
  const [showCreateModal, setShowCreateModal] = useState(false);
  const [showUpdateModal, setShowUpdateModal] = useState(false);
  const [showEnrollModal, setShowEnrollModal] = useState(false);
  const [showRequirements, setshowRequirements] = useState(false);
  const [showPreEnrollModal, setshowPreEnrollModal] = useState(false);
  const [showWhyModal, setShowWhyModal] = useState(false);
  const { user } = useContext(UserContext);
  const [isBusy, setBusy] = useState(false);
  const [searchText, setsearchText] = useState("");
  const [listOfRequirements, setlistOfRequirements] = useState([]);

  const handleClose = () => {
    setShowCreateModal(false);
    setShowUpdateModal(false);
    setShowEnrollModal(false);
    setShowWhyModal(false);
    setshowRequirements(false);
    setshowPreEnrollModal(false);
    setfirstname("");
    setlastname("");
    setage("");
    setemail("");
    setgender("");
    setbirthday("");
    setaddress("");
    setmobileNo("");
    setparentsName("");
    setparentsMobileNo("");
    setStudentId("");
    setusername("");
    setpassword("");
    setremarks("");
    settotalAmount("");
  };

  //data student

  const [firstname, setfirstname] = useState("");
  const [lastname, setlastname] = useState("");
  const [fullName, setFullName] = useState("");
  const [age, setage] = useState("");
  const [birthday, setbirthday] = useState("");
  const [gender, setgender] = useState("");
  const [address, setaddress] = useState("");
  const [email, setemail] = useState("");
  const [mobileNo, setmobileNo] = useState("");
  const [parentsName, setparentsName] = useState("");
  const [parentsMobileNo, setparentsMobileNo] = useState("");
  const [studentId, setStudentId] = useState("");
  const [username, setusername] = useState("");
  const [password, setpassword] = useState("");
  const [totalAmount, settotalAmount] = useState(0);
  const [courseId, setcourseId] = useState("");
  const [sectionId, setsectionId] = useState("");
  const [whyDecline, setwhyDecline] = useState("");
  const [remarks, setremarks] = useState("");

  useEffect(() => {
    getAllStudent();
  }, [renders]);
  //TODO Get all student
  async function getAllStudent() {
    setData(await GetAllStudent());
  }
  //TODO get one student
  async function getOneStudent(studentId) {
    setBusy(true);
    const result = await GetOneStudent(studentId);
    setfirstname(result.firstname);
    setlastname(result.lastname);
    setFullName(result.firstname + " " + result.lastname);
    setage(result.age);
    setaddress(result.address);
    setmobileNo(result.mobileNo);
    setemail(result.email);
    setStudentId(studentId);
    setcourseId(result.courseId);
    setsectionId(result.sectionId);
    setBusy(false);
  }

  async function getNameClassOrCourse() {
    let name = "";
    if (sectionId !== "") {
      const result = await GetOneSection(sectionId);
      name = result.name;
    } else {
      const result = await GetOneCourse(courseId);
      name = result.name;
    }
    return name;
  }

  //TODO send eamil
  async function sendEnrolledEmail() {
    setBusy(true);
    const name = await getNameClassOrCourse();
    const result = await SendEmailToApplicant(
      `Hello ${firstname}, thank you for choosing us. You have selected ${name} with a Total Amount of PHP ${totalAmount}. You may pay in our Bursar Office; opens at 9 am to 6 pm only. After such, once your payment is successfully done, you’re automatically enrolled to our school with your selected Course/Section Name. Please login to your account and check your Student Dashboard for your admission status. Thank you!`,
      fullName,
      "raven5svg@gmail.com"
    );
    if (result) {
      //TODO Create Payment
      const paymentRequest = await CreatePayment(
        firstname + " " + lastname,
        totalAmount,
        studentId,
        remarks,
        user.id
      );
      if (paymentRequest) {
        setBusy(false);
        swal("Payment Created and Email Sent", {
          icon: "success",
        });
      } else {
        setBusy(false);
        swal("Opss! Something error occured when updating", {
          icon: "error",
        });
      }

      handleClose();
    }

    return result;
  }

  // ! Pre enrollment payment
  //TODO send eamil
  async function sendPreEnrollEmail() {
    setBusy(true);
    const result = await SendEmailToApplicant(
      `Hello ${firstname}, thank you for choosing us. You have been accepted with a Total Amount of PHP ${totalAmount} as a Pre-Enrollment Payment. You may pay in our Bursar Office; opens at 9 am to 6 pm only. Once your payment is successfully done, you will be able to login and enroll to our school with your selected Course or Class. Thank you!`,
      fullName,
      email
    );
    if (result) {
      //TODO Create Payment
      const paymentRequest = await CreatePayment(
        firstname + " " + lastname,
        totalAmount,
        studentId,
        "Pre-Enrollment",
        user.id
      );
      if (paymentRequest) {
        setBusy(false);
        swal("Student has been accepted and Email sent", {
          icon: "success",
        });
      } else {
        setBusy(false);
        swal("Opss! Something error occured when updating", {
          icon: "error",
        });
      }

      handleClose();
    }
    getAllStudent();
    return result;
  }
  // ! End of Pre Enrollment Payment

  //TODO send eamil
  async function sendUnenrolledEmail() {
    setBusy(true);
    const result = await SendEmailToApplicant(
      `Hi ${firstname}, Due to ${whyDecline} we are unable to process your admission/enrollment and display your account.  To access this portal? check the information given or contact the Bursar/Registrar office to for more information and clarification. You may also contact us at phone number or email add. Thank you!`,
      fullName,
      "raven5svg@gmail.com"
    );
    setBusy(false);
    if (result)
      swal("Email Sent!", {
        icon: "success",
      });
    else
      swal("Sending email failed", {
        icon: "error",
      });
    getAllStudent();
  }

  //TODO create student
  async function handleCreate() {
    setBusy(true);
    const result = await CreateStudent(
      firstname,
      lastname,
      age,
      gender,
      birthday,
      address,
      mobileNo,
      parentsName,
      parentsMobileNo,
      email,
      username,
      password
    );
    setBusy(false);
    if (result)
      swal("Poof! New Student has been Created!", {
        icon: "success",
      });
    getAllStudent();
    handleClose();
  }

  async function handleDelete(studentId) {
    const willDelete = await swal({
      title: "Are you sure?",
      text: "Once deleted, you will not be able to recover this student!",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    });

    if (willDelete) {
      setBusy(true);
      const result = await DeleteStudent(studentId);
      setBusy(false);
      if (result) {
        swal("Poof! Student has been deleted!", {
          icon: "success",
        });
        handleClose();
      } else {
        swal("Opss! Student deletion failed!", {
          icon: "error",
        });
        handleClose();
      }
    } else {
      handleClose();
    }
    getAllStudent();
  }

  //TODO update student
  async function handleUpdate() {
    setBusy(true);
    const result = await UpdateStudent(
      studentId,
      firstname,
      lastname,
      age,
      address,
      email,
      mobileNo
    );
    setBusy(false);
    if (result)
      swal("Poof! Student has been updated!", {
        icon: "success",
      });
    handleClose();
    getAllStudent();
  }
  //TODO delete a student
  //TODO Enroll a Student to a Section or Course

  const filteredStudents = data.filter((student) => {
    if (searchText === "") {
      return student;
    } else {
      return (
        student.firstname.toLowerCase().indexOf(searchText.toLowerCase()) !== -1
      );
    }
  });

  const showEnrolledStudent = filteredStudents.map((student) => {
    if (student.status === "Enrolled") {
      return (
        <tr key={student._id}>
          <td>{student.firstname + " " + student.lastname}</td>
          <td>{student.age}</td>
          <td>{student.gender}</td>
          <td>{student.mobileNo}</td>
          <td>{student.email}</td>
          <td>
            <div className="d-flex justify-content-around">
              <Button
                onClick={() => {
                  setShowUpdateModal(true);
                  getOneStudent(student._id);
                }}
                variant="warning"
              >
                Update
              </Button>
              <Button
                variant="danger"
                onClick={() => handleDelete(student._id)}
              >
                Delete
              </Button>
            </div>
          </td>
        </tr>
      );
    }
  });
  const showPendingEnrolledStudent = filteredStudents.map((student) => {
    if (student.status === "Pre-Enrolled") {
      return (
        <tr key={student._id}>
          <td>{student.firstname + " " + student.lastname}</td>
          <td>{student.age}</td>
          <td>{student.gender}</td>
          <td>{student.mobileNo}</td>
          <td>{student.email}</td>
          <td>
            <div className="d-flex justify-content-around">
              <Button
                onClick={() => {
                  setShowUpdateModal(true);
                  getOneStudent(student._id);
                }}
                variant="warning"
              >
                Update
              </Button>
              <Button
                variant="info"
                onClick={() => {
                  setShowEnrollModal(true);
                  getOneStudent(student._id);
                }}
              >
                Enroll
              </Button>
              <Button
                variant="danger"
                onClick={() => handleDelete(student._id)}
              >
                Delete
              </Button>
            </div>
          </td>
        </tr>
      );
    }
  });

  const showNotEnrolledStudent = filteredStudents.map((student) => {
    if (student.status === "Not Enrolled") {
      return (
        <tr key={student._id}>
          <td>{student.firstname + " " + student.lastname}</td>
          <td>{student.age}</td>
          <td>{student.gender}</td>
          <td>{student.mobileNo}</td>
          <td>{student.email}</td>
          <td>
            <div className="d-flex justify-content-around">
              <Button
                onClick={() => {
                  setShowUpdateModal(true);
                  getOneStudent(student._id);
                }}
                variant="warning"
              >
                Update
              </Button>
              <Button
                variant="danger"
                onClick={() => handleDelete(student._id)}
              >
                Delete
              </Button>
            </div>
          </td>
        </tr>
      );
    }
  });

  const showNotAdmittedStudent = filteredStudents.map((student) => {
    if (student.status === "Not Admitted") {
      return (
        <tr key={student._id}>
          <td>{student.firstname + " " + student.lastname}</td>
          <td>{student.age}</td>
          <td>{student.gender}</td>
          <td>{student.mobileNo}</td>
          <td>{student.email}</td>
          <td>
            <div className="d-flex justify-content-around">
              <Button
                onClick={() => {
                  setlistOfRequirements(student.requirements);
                  setshowRequirements(true);
                  getOneStudent(student._id);
                }}
                variant="info"
              >
                View Requirements
              </Button>
              <Button
                onClick={() => {
                  setShowUpdateModal(true);
                  getOneStudent(student._id);
                }}
                variant="warning"
              >
                Update
              </Button>
              <Button
                variant="danger"
                onClick={() => handleDelete(student._id)}
              >
                Delete
              </Button>
            </div>
          </td>
        </tr>
      );
    }
  });

  const requirements = listOfRequirements.map((requirement) => {
    return (
      <tr key={requirement._id}>
        <td>{requirement.name}</td>
        <td>
          <a href={requirement.link} target="_blank">
            <Button variant="success"> Link </Button>
          </a>
        </td>
      </tr>
    );
  });

  if (isBusy) return <Loading />;
  else
    return (
      <>
        <h1>Student</h1>
        <Row className="justify-content-center">
          <Col md={10}>
            <Row className="d-flex justify-content-between">
              <Button className="my-2" onClick={() => setShowCreateModal(true)}>
                Add New Student
              </Button>
              <TextField
                type="text"
                textValue={searchText}
                onChange={(e) => setsearchText(e.target.value)}
                placeholder="Search Student"
                controlId="SearchId3"
              />
            </Row>
            <h3>Pre-Enrolled</h3>
            <Table striped bordered hover>
              <thead>
                <tr>
                  <th>Name</th>
                  <th>Age</th>
                  <th>Gender</th>
                  <th>Mobile No.</th>
                  <th>Email</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>{showPendingEnrolledStudent}</tbody>
            </Table>
            <hr></hr>
            <h3>Enrolled</h3>
            <Table striped bordered hover>
              <thead>
                <tr>
                  <th>Name</th>
                  <th>Age</th>
                  <th>Gender</th>
                  <th>Mobile No.</th>
                  <th>Email</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>{showEnrolledStudent}</tbody>
            </Table>
            <hr></hr>
            <h3>Not Enrolled</h3>
            <Table striped bordered hover>
              <thead>
                <tr>
                  <th>Name</th>
                  <th>Age</th>
                  <th>Gender</th>
                  <th>Mobile No.</th>
                  <th>Email</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>{showNotEnrolledStudent}</tbody>
            </Table>
            <hr></hr>
            <h3>Not Admitted</h3>
            <Table striped bordered hover>
              <thead>
                <tr>
                  <th>Name</th>
                  <th>Age</th>
                  <th>Gender</th>
                  <th>Mobile No.</th>
                  <th>Email</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>{showNotAdmittedStudent}</tbody>
            </Table>
          </Col>
        </Row>
        <Modal
          show={showUpdateModal}
          onHide={handleClose}
          centered
          size="lg"
          aria-labelledby="updateModal"
        >
          <Modal.Header closeButton>
            <Modal.Title>Update Student</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <TextField
              label="First Name"
              type="text"
              textValue={firstname}
              onChange={(e) => setfirstname(e.target.value)}
              placeholder="First Name"
              controlId="firstnameId"
            />
            <TextField
              label="Last Name"
              type="text"
              textValue={lastname}
              onChange={(e) => setlastname(e.target.value)}
              placeholder="Last Name"
              controlId="lastnameId"
            />
            <TextField
              label="Age"
              type="text"
              textValue={age}
              onChange={(e) => setage(e.target.value)}
              placeholder="Age"
              controlId="ageId"
            />
            <TextField
              label="Address"
              type="text"
              textValue={address}
              onChange={(e) => setaddress(e.target.value)}
              placeholder="Address"
              controlId="addressId"
            />
            <TextField
              label="Email Address"
              type="text"
              textValue={email}
              onChange={(e) => setemail(e.target.value)}
              placeholder="Email Address"
              controlId="emailId"
            />
            <TextField
              label="Mobile No"
              type="text"
              textValue={mobileNo}
              onChange={(e) => setmobileNo(e.target.value)}
              placeholder="Mobile No"
              controlId="mobileNoid"
            />
          </Modal.Body>
          <Modal.Footer>
            <Button variant="secondary" onClick={handleClose}>
              Close
            </Button>
            <Button variant="primary" onClick={handleUpdate}>
              Update Student
            </Button>
          </Modal.Footer>
        </Modal>
        <Modal
          show={showCreateModal}
          onHide={handleClose}
          centered
          size="lg"
          aria-labelledby="createModal"
        >
          <Modal.Header closeButton>
            <Modal.Title>Create Student</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <TextField
              label="First name"
              type="text"
              textValue={firstname}
              onChange={(e) => setfirstname(e.target.value)}
              placeholder="First Name"
              controlId="firstnameId"
            />
            <TextField
              label="Last name"
              type="text"
              textValue={lastname}
              onChange={(e) => setlastname(e.target.value)}
              placeholder="Last Name"
              controlId="lastnameId"
            />
            <TextField
              label="Age"
              type="text"
              textValue={age}
              onChange={(e) => setage(e.target.value)}
              placeholder="Age"
              controlId="ageId"
            />
            <TextField
              label="Birthday"
              type="text"
              textValue={birthday}
              onChange={(e) => setbirthday(e.target.value)}
              placeholder="YYYY-MM-DD use this format"
              controlId="birthdayId"
            />
            <Form.Control
              as="select"
              className="selectControl mb-3"
              onChange={(e) => setgender(e.target.value)}
            >
              <option>Choose Gender</option>
              <option>Male</option>
              <option>Female</option>
            </Form.Control>
            <TextField
              label="address"
              type="text"
              textValue={address}
              onChange={(e) => setaddress(e.target.value)}
              placeholder="Address"
              controlId="addressId"
            />
            <TextField
              label="Email Address"
              type="text"
              textValue={email}
              onChange={(e) => setemail(e.target.value)}
              placeholder="Email Address"
              controlId="emailId"
            />
            <TextField
              label="Mobile Number"
              type="text"
              textValue={mobileNo}
              onChange={(e) => setmobileNo(e.target.value)}
              placeholder="Mobile No"
              controlId="mobileNoId"
            />
            <TextField
              label="Parent/s Name"
              type="text"
              textValue={parentsName}
              onChange={(e) => setparentsName(e.target.value)}
              placeholder="Parents Name"
              controlId="parentNameId"
            />
            <TextField
              label="Parent/s Mobile Number"
              type="text"
              textValue={parentsMobileNo}
              onChange={(e) => setparentsMobileNo(e.target.value)}
              placeholder="Parents Mobile No"
              controlId="parentMobileId"
            />
            <TextField
              label="Username"
              type="text"
              textValue={username}
              onChange={(e) => setusername(e.target.value)}
              placeholder="Username"
              controlId="usernameId"
            />
            <TextField
              label="Password"
              type="password"
              textValue={password}
              onChange={(e) => setpassword(e.target.value)}
              placeholder="Password"
            />
          </Modal.Body>
          <Modal.Footer>
            <Button variant="secondary" onClick={handleClose}>
              Close
            </Button>
            <Button variant="primary" onClick={handleCreate}>
              Create Student
            </Button>
          </Modal.Footer>
        </Modal>
        <Modal
          show={showEnrollModal}
          onHide={handleClose}
          centered
          size="lg"
          aria-labelledby="updateModal"
        >
          <Modal.Header closeButton>
            <Modal.Title>Approval of Pre-Enrollments</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <p>
              <strong>Firstname: </strong>
              {firstname}
            </p>
            <p>
              <strong>Lastname: </strong>
              {lastname}
            </p>
            <p>
              <strong>Age: </strong>
              {age}
            </p>

            <p>
              <strong>Address: </strong>
              {address}
            </p>
            <p>
              <strong>Email Address: </strong>
              {email}
            </p>
            <p className="mb-5">
              <strong>Mobile No: </strong>
              {mobileNo}
            </p>
            <Row>
              <Col md={6}>
                <strong>Total School Fee </strong>
              </Col>
              <Col md={6}>
                <TextField
                  label="Class/Course Amount Fee"
                  type="Number"
                  textValue={totalAmount}
                  onChange={(e) => settotalAmount(e.target.value)}
                  placeholder="Total Amount"
                  controlId="totalAmountId"
                />
                <Form.Label>Remarks</Form.Label>
                <Form.Control
                  as="textarea"
                  rows={3}
                  placeholder="Remarks if accepted only"
                  onChange={(e) => setremarks(e.target.value)}
                />
              </Col>
            </Row>
          </Modal.Body>
          <Modal.Footer>
            <Button
              variant="success"
              disabled={totalAmount < 1 ? true : false}
              onClick={() => sendEnrolledEmail()}
            >
              Accept Enrollment
            </Button>
            <Button
              variant="danger"
              onClick={() => {
                setShowEnrollModal(false);
                setShowWhyModal(true);
              }}
            >
              Decline Enrollment
            </Button>
          </Modal.Footer>
        </Modal>
        <Modal
          show={showWhyModal}
          onHide={handleClose}
          centered
          size="lg"
          aria-labelledby="updateModal"
        >
          <Modal.Header closeButton>
            <Modal.Title>Why?</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <Form.Control
              as="select"
              onChange={(e) => setwhyDecline(e.target.value)}
            >
              <option>-</option>
              <option>Enrollment Limit</option>
              <option>Lack of Documents</option>
              <option>Wrong Account Used</option>
            </Form.Control>
          </Modal.Body>
          <Modal.Footer>
            <Button variant="success" onClick={handleClose}>
              Cancel
            </Button>
            <Button
              variant="danger"
              onClick={() => {
                sendUnenrolledEmail();
                handleClose();
              }}
            >
              Decline Enrollment
            </Button>
          </Modal.Footer>
        </Modal>
        <Modal
          show={showRequirements}
          onHide={handleClose}
          centered
          size="lg"
          aria-labelledby="requirementsModal"
        >
          <Modal.Header closeButton>
            <Modal.Title>Requirements</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <Table striped bordered hover>
              <thead>
                <tr>
                  <th>Name</th>
                  <th>Link</th>
                </tr>
              </thead>
              <tbody>{requirements}</tbody>
            </Table>
          </Modal.Body>
          <Modal.Footer>
            <Button
              variant="success"
              onClick={() => setshowPreEnrollModal(true)}
            >
              Accept Student
            </Button>
            <Button variant="danger" onClick={handleClose}>
              Decline student
            </Button>
          </Modal.Footer>
        </Modal>
        <Modal
          show={showPreEnrollModal}
          onHide={handleClose}
          centered
          size="xs"
          aria-labelledby="requirementsModal"
        >
          <Modal.Header closeButton>
            <Modal.Title>Pre-Enrollment</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <TextField
              label="Pre-Enrollment Fee"
              type="Number"
              textValue={totalAmount}
              onChange={(e) => settotalAmount(e.target.value)}
              placeholder="Pre-Enrollment Fee"
              controlId="totalAmountId"
            />
          </Modal.Body>
          <Modal.Footer>
            <Button variant="success" onClick={() => sendPreEnrollEmail()}>
              Submit
            </Button>
            <Button
              variant="danger"
              onClick={() => setshowPreEnrollModal(false)}
            >
              Cancel
            </Button>
          </Modal.Footer>
        </Modal>
      </>
    );
};

export default Student;
