import { Navbar, Nav } from "react-bootstrap";

import Link from "next/link";
import styles from "./navbar-style.module.css";
import { useContext } from "react";
import UserContext from "../../context/UserContext";

export default function NavBar() {
  const { user } = useContext(UserContext);

  return (
    <>
      <Navbar bg="light" variant="light" expand="lg" className={styles.navbar}>
        <Navbar.Brand href="/dashboard/student">
          Holy Cross College of Carigara
        </Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="ml-auto ">
            {user.status === "Enrolled" ? (
              <Link href="/profile">
                <a className="nav-link">Profile</a>
              </Link>
            ) : (
              <div></div>
            )}
            {user.id == null ? (
              <Link href="/">
                <a className="nav-link">Login</a>
              </Link>
            ) : (
              <Link href="/logout">
                <a className="nav-link">Logout</a>
              </Link>
            )}
          </Nav>
        </Navbar.Collapse>
      </Navbar>
    </>
  );
}
