/* eslint-disable no-unused-vars */
/** npm packages */
import Head from "next/head";

const HeadDefault = (props) => {
  return (
    <Head>
      <title>{props.title}</title>

      <link rel="icon" href="/favicon.ico" />

      {/* META SEO */}
      <meta name="title" content={props.title} />
      <meta name="description" content={props.description} />
    </Head>
  );
};

export default HeadDefault;
